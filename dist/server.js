"use strict";

require('dotenv').config();

var express = require('express');

var compression = require('compression');

var app = express();
var port = process.env.PORT || 5000;

var path = require('path');

var fs = require('fs');

var resortService = require('./services/resort').default;

var url = process.env.API_URL;
var filePath = path.resolve(__dirname, '../build', 'index.html');
var indexData = fs.readFileSync(filePath, 'utf8');
app.use(compression());

var home = function home(request, response) {
  console.log('Home page visited!');
  response.send(defaultHeader(indexData));
};

app.get('/', home);
app.get('/home', home);

var defaultHeader = function defaultHeader(data) {
  data = data.replace(/\$OG_TITLE/g, 'Camellia');
  data = data.replace(/\$OG_DESCRIPTION/g, "Camellia Tam Đảo");
  return data.replace(/\$OG_IMAGE/g, url + '/img/index/slide-1.jpg');
};

var readFile = function readFile(callback) {
  callback(indexData);
};

var roomsController = function roomsController(request, response) {
  console.log('rooms page visited!', request.params);
  readFile(function (data) {
    data = data.replace(/\$OG_TITLE/g, 'Rooms');
    data = data.replace(/\$OG_DESCRIPTION/g, 'Rooms');
    var result = data.replace(/\$OG_IMAGE/g, url + '/img/index/slide-1.jpg');
    response.send(result);
  });
};

var pagesController = function pagesController(request, response) {
  console.log('pages page visited!', request.params);
  readFile(function (data) {
    var id = request.params.id;
    resortService.getBySlug(id).then(function (pageData) {
      if (pageData) {
        data = data.replace(/\$OG_TITLE/g, pageData.title);

        if (pageData.description) {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.description);
        } else {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.title);
        }

        var result = data.replace(/\$OG_IMAGE/g, pageData.thumbnail);
        response.send(result);
      } else {
        response.send(defaultHeader(data));
      }
    }).catch(function (err) {
      console.log(err);
      response.send(data);
    });
  });
};

app.get('/pages/:id', pagesController);
app.get('/rooms', roomsController);

var roomItemController = function roomItemController(request, response) {
  console.log('roomItem page visited!', request.params);
  readFile(function (data) {
    var id = request.params.id;
    resortService.getRoomById(id).then(function (pageData) {
      if (pageData) {
        data = data.replace(/\$OG_TITLE/g, pageData.name);

        if (pageData.description) {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.description);
        } else {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.name);
        }

        var result = data.replace(/\$OG_IMAGE/g, pageData.thumbnail);
        response.send(result);
      } else {
        response.send(defaultHeader(data));
      }
    }).catch(function (err) {
      response.send(data);
    });
  });
};

app.get('/rooms/:id/:title', roomItemController);
app.get('/contact', function (request, response) {
  console.log('Contact page visited!');
  var filePath = path.resolve(__dirname, './build', 'index.html');
  fs.readFile(filePath, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }

    data = data.replace(/\$OG_TITLE/g, 'Contact Page');
    data = data.replace(/\$OG_DESCRIPTION/g, "Contact page description");
    var result = data.replace(/\$OG_IMAGE/g, 'https://i.imgur.com/V7irMl8.png');
    response.send(result);
  });
});
app.use(express.static(path.resolve(__dirname, '../build')));
app.get('*', function (request, response) {
  var filePath = path.resolve(__dirname, '../build', 'index.html');
  response.sendFile(filePath);
});
app.listen(port, function () {
  return console.log("Listening on port ".concat(port));
});