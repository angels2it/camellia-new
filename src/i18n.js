
import i18n from "i18next";
import { initReactI18next } from "react-i18next";
const vi = require("./locales/vi/translation.json");
const en = require("./locales/en/translation.json");

const locale = localStorage.getItem('locale') || 'en';

const instance = i18n
    .use(initReactI18next) // passes i18n down to react-i18next
    .init({
        resources: {
            vi: {
                translation: vi,
            },
            en: {
                translation: en,
            },
        },
        lng: locale,
        fallbackLng: "en",

        interpolation: {
            escapeValue: false,
        },

        loadPath: "/locales/{{lng}}/{{ns}}.json",
    });

export default i18n;