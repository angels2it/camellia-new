import React, { useState, useEffect, useMemo, useContext } from "react";
import _ from "lodash";
import OwlCarousel from "react-owl-carousel";
import { Row, Col, Card, List, Form, Input, Button, Modal, Typography, Descriptions } from 'antd';
import { useParams } from "react-router-dom";
import resortService from "./../services/resort";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useTranslation } from "react-i18next";
import { FacebookShareButton, FacebookIcon, TwitterIcon, TwitterShareButton, PinterestIcon, PinterestShareButton, EmailIcon, EmailShareButton } from 'react-share';
import ImageGallery from "react-image-gallery";
import moment from 'moment';

import "react-image-gallery/styles/css/image-gallery.css";

import Header from "./../components/header";
import Footer from "./../components/footer";

import "./page.scss";
import resort from "./../services/resort";
import SettingContext from "../contexts/setting";
import RoomItem from "../components/room-item";
import { Helmet } from "react-helmet";
import { countDiscount, formatCurrency } from "../helpers/utils";
import NewsItems from "../components/news-item";
import FacilityItem from "../components/facility-item";

const SELF_HANDLE_ID = ['contact'];
const SELF_HANDLE_TYPE = ['rooms', 'news', 'facility', 'service'];

function Page({ changeLanguage }) {
  const [item, setItem] = useState();
  const [rooms, setRooms] = useState([]);
  const [photos, setPhotos] = useState([]);
  const [roomUrl, setRoomUrl] = useState();
  let { id, type } = useParams();

  const setting = useContext(SettingContext);
  const { t } = useTranslation();

  useEffect(() => {
    if (id) {
      switch (type) {
        case "facility":
          resortService.getFacility(id).then((data) => {
            setItem(data);
          });
          break;
        case "service":
          resortService.getFacility(id).then((data) => {
            setItem(data);
          });
          break;
        case "news":
          resortService.getNewsItem(id).then((data) => {
            setItem(data);
          });
          break;
        case "pages":
          resortService.getBySlug(id).then((data) => {
            setItem(data);
          });
          break;

        case "rooms":
          resortService.getRoomWithCurrencyById(id, setting.currency).then((data) => {
            setItem(data);
          });
          break;

        default:
          break;
      }
    }
  }, [id]);

  useEffect(() => {
    if (item && type === 'rooms') {
      var url = `${process.env.URL}/${setting.locale}/rooms/${item.id}/${item.name}`;
      setRoomUrl(url);

      const photos = _.map(item.albums, (item, index) => ({
        original: item.url,
        alt: item.name,
        thumbnail: item.url,
      }));

      setPhotos(photos);
      resortService.getRooms({ exceptId: item.id, checkInDate: moment().startOf('d').utc().format(), checkOutDate: moment().add(1, 'd').endOf('d').utc().format(), currency: setting.currency }).then(r => {
        setRooms(_.get(r, 'items', []));
      })
    }
  }, [item]);

  return (
    <>
      <Helmet>
        {item && <title>{item.name || item.title}</title>}
      </Helmet>
      <Header changeLanguage={changeLanguage} />
      {item && (
        <div className="banner">
          <OwlCarousel
            className="owl-theme"
            loop
            nav
            dots
            responsive={{
              0: {
                items: 1,
              },
              600: {
                items: 1,
              },
              1000: {
                items: 1,
              },
            }}
            id="sliderBanner"
          >
            <div className="item">
              <div className="banner__item">
                <div className="banner__img">
                  <img src={item.thumbnail} alt={item.title} />
                </div>
                <div className="banner__content">
                  <div className="container">
                    <div className="star">
                      <i className="fa fa-star" aria-hidden="true" />
                      <i className="fa fa-star" aria-hidden="true" />
                      <i className="fa fa-star" aria-hidden="true" />
                      <i className="fa fa-star" aria-hidden="true" />
                      <i className="fa fa-star" aria-hidden="true" />
                    </div>
                    <h1 className="title" style={{ color: "white" }}>
                      {item.title || item.name}
                    </h1>
                    {item.description && <p className="text-banner">{item.description}</p>}
                    {item.bedType && <p className="text-banner">{item.bedType.name}</p>}
                  </div>
                </div>
              </div>
            </div>
          </OwlCarousel>
        </div>
      )}
      <main id="content">
        {item && type === 'rooms' &&
          <>
            <Row align="middle" style={{ marginBottom: 20 }} gutter={[10, 0]}>
              <Col style={{ textAlign: 'center' }} span={24} className="">
                <FacebookShareButton url={roomUrl} style={{ marginRight: 20 }}>
                  <FacebookIcon size={32} round />
                </FacebookShareButton>
                <TwitterShareButton url={roomUrl} style={{ marginRight: 20 }}>
                  <TwitterIcon size={32} round />
                </TwitterShareButton>
                <PinterestShareButton url={roomUrl} style={{ marginRight: 20 }}>
                  <PinterestIcon size={32} round />
                </PinterestShareButton>
                <EmailShareButton url={roomUrl}>
                  <EmailIcon size={32} round />
                </EmailShareButton>
              </Col>
            </Row>
          </>}
        {item && !_.includes(SELF_HANDLE_ID, id) && !_.includes(SELF_HANDLE_TYPE, type) && item.content && (
          <div
            className="content__editor"
            dangerouslySetInnerHTML={{ __html: item.content }}
          />
        )}
        {item && id === 'contact' && (
          <>
            <Row>
              <Col md={{ span: 8, offset: 4 }} sm={24}>
                {item.content && <div
                  className="content__editor"
                  dangerouslySetInnerHTML={{ __html: item.content }}
                />}
              </Col>
              <Col md={8} sm={24} xs={24}>
                <ContactForm />
              </Col>
            </Row>
            <Row align="middle" style={{ marginTop: 30 }}>
              <Col md={{ span: 16, offset: 4 }} xs={{ span: 24 }}>
                <iframe
                  width="100%"
                  height="450"
                  frameborder="0"
                  style={{ border: 0 }}
                  src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyBEIr2KBfdU8ZT3Y_qqi63zVuILBDuUSPA&q=${setting['App.UI.Address']}`} allowfullscreen>
                </iframe>
              </Col>
            </Row>
          </>
        )}

        {item && type === 'rooms' && (
          <>
            <Row gutter={[20, 20]} style={{ border: '5px solid rgba(241, 196, 15, 0.2)' }}>
              <Col md={{ span: 18 }} sm={24}>
                {_.size(photos) > 0 && <ImageGallery items={photos} showNav={true} autoPlay={true} showBullets={true} showThumbnails={false} showPlayButton={false} />}
              </Col>
              <Col md={6} sm={24} xs={24}>
                {item.content && <div
                  className="content__editor"
                  dangerouslySetInnerHTML={{ __html: item.content }}
                />}
              </Col>
            </Row>
            <RoomAttribute item={item} />
          </>
        )}

        {item && type === 'rooms' && (
          <>
            <hr />
            <Row>
              <Typography.Title>{t('room.other')}</Typography.Title>
            </Row>
            <section className="rooms rooms-widget rooms-widget-nobackground">
              <div className="">
                <div className="row">
                  {_.map(rooms, (item, index) => (
                    <div key={index} className="col-md-4">
                      <RoomItem item={item} locale={setting.locale}></RoomItem>
                    </div>
                  ))}
                </div>
              </div>
            </section>
          </>)}
        {item && type === 'news' && (
          <>
            <Row gutter={[20, 20]}>
              <Col md={{ span: 16 }} xs={{ span: 24 }}>
                <div className="info" style={{ marginBottom: 10 }}>
                  {item.creatorUser && <span>{item.creatorUser.name}</span>}{' • '}
                  <span>{moment.utc(item.creationTime).local().format('hh:mm DD/MM/YYYY')}</span>
                </div>
                <div
                  className="content__editor"
                  dangerouslySetInnerHTML={{ __html: item.content }}
                />
              </Col>
              <Col md={{ span: 6 }} xs={{ span: 24 }}>
                <RecentNews id={item.id} />
              </Col>
            </Row>
          </>
        )}
        {item && (type === 'facility' || type === 'service') && (
          <>
            <Row gutter={[20, 20]}>
              <Col md={{ span: 16 }} xs={{ span: 24 }}>
                <div
                  className="content__editor"
                  style={{ display: 'table-column' }}
                >
                  <div dangerouslySetInnerHTML={{ __html: item.content }} />
                </div>
              </Col>
              <Col md={{ span: 6 }} xs={{ span: 24 }}>
                <RecentFacility id={item.id} />
              </Col>
            </Row>
          </>
        )}
      </main>
      <Footer />
    </>
  );
}


const RoomAttribute = ({ item }) => {
  const { t } = useTranslation();

  const setting = useContext(SettingContext);
  const { currency, pricingItems, attributes } = item;

  const price = useMemo(() => {
    var minPriceItem = _.minBy(pricingItems, ({ price, discount, discountType, }) => {
      return countDiscount(price, discount, discountType);
    }) || {};
    return minPriceItem;
  }, [pricingItems]);
  return (
    <Row gutter={[10, 20]}>
      <Col span={24}>
        {_.size(attributes) > 0 && attributes.map((a, index) => <span className="room-attribute" key={index}>{a.attribute.icon && <FontAwesomeIcon key={index} icon={a.attribute.icon} />} {a.attribute.name} {a.value ? (' - ' + a.value) : ''}
        </span>)}
        <div style={{ float: "right", display: 'flex' }}>
          <div style={{ marginTop: 5 }}>
            <span
              className={
                price.discount > 0 ? "price has-discount" : "price"
              }
              style={{ fontWeight: 500, fontFamily: 'sans-serif' }}
            >
              {formatCurrency(price.price, currency)}
            </span>{" "}
            {price.discount > 0 && (
              <span className="price-discount" style={{ fontWeight: 500, fontFamily: 'sans-serif' }}>
                {formatCurrency(
                  countDiscount(price.price, price.discount, price.discountType),
                  currency
                )}
              </span>
            )}
            <span className="text-small" style={{ whiteSpace: 'pre' }}> {t('per_night')}</span>
          </div>

          <a className="btn btn-main" href={`/${setting.locale}/booking`} style={{
            marginLeft: 20, backgroundColor: '#00486c',
            textTransform: 'uppercase',
            color: '#fff',
            borderRadius: 0,
            padding: '5px 5px',
            fontSize: 14,
          }}>{t('book_now')}</a>
        </div>
      </Col>
    </Row>
  )
}

const ContactForm = () => {
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const rules = {
    title: [{ required: true, message: t('feedback.validate.title') }],
    guestName: [{ required: true, message: t('feedback.validate.guestName') }],
    guestEmail: [{ required: true, message: t('feedback.validate.guestEmail') }, { type: 'email', message: (t('guest_email_is_invalid')) }],
    guestPhone: [{ required: true, message: t('feedback.validate.guestPhone') }],
    message: [{ required: true, message: t('feedback.validate.message') }],
  }
  const handleFeedback = (values) => {
    resort.sendFeedback(values)
      .then((result) => {
        if (result) {
          Modal.success({ title: t('feedback.success'), content: t('feedback.successContent') });
          form.resetFields();
        } else {
          Modal.error({ title: t('feedback.error'), content: t('feedback.errorContent') })
        }
      });
  }
  return (
    <Form form={form} onFinish={handleFeedback} style={{ width: '100%' }}>
      <Form.Item name="title" rules={rules.title}>
        <Input placeholder={t('feedback.title')} style={{ width: '100%' }} />
      </Form.Item>
      <Form.Item name="guestName" rules={rules.guestName}>
        <Input placeholder={t('feedback.guestName')} style={{ width: '100%' }} />
      </Form.Item>
      <Form.Item name="guestPhone" rules={rules.guestPhone}>
        <Input placeholder={t('feedback.guestPhone')} style={{ width: '100%' }} />
      </Form.Item>
      <Form.Item name="guestEmail" rules={rules.guestEmail}>
        <Input placeholder={t('feedback.guestEmail')} style={{ width: '100%' }} />
      </Form.Item>
      <Form.Item name="message" rules={rules.message}>
        <Input.TextArea rows={5} placeholder={t('feedback.message')} style={{ width: '100%' }} />
      </Form.Item>
      <Button type="primary" htmlType="submit">{t('feedback.send')}</Button>
    </Form>
  )
}

const RecentNews = ({ id }) => {
  const [news, setNews] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const { t } = useTranslation();
  useEffect(() => {
    if (id > 0) {
      setIsLoading(true);
      resortService.getNews(5, id)
        .then(r => {
          setIsLoading(false);
          setNews(_.get(r, 'items', []));
        })
        .catch(e => {
          setIsLoading(false);
        });
    }
  }, [id]);
  return (
    <Card loading={isLoading} bodyStyle={{ padding: 0 }} title={t('news_recent')}>
      <section className="new new-widget-nobackground" id="news-list" style={{ paddingTop: 20, paddingBottom: 0 }}>
        <div className="container">
          <div className="row">
            {_.map(news, (item, index) => (
              <div key={index} className="col-md-12">
                <NewsItems item={item}></NewsItems>
              </div>
            ))}
          </div>
        </div>
      </section>
    </Card>
  )
}

const RecentFacility = ({ id }) => {
  const [items, setItems] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  const { t } = useTranslation();
  const setting = useContext(SettingContext);
  useEffect(() => {
    if (id > 0) {
      setIsLoading(true);
      resortService.getFacilities(null, 5, id)
        .then(r => {
          setIsLoading(false);
          setItems(_.get(r, 'items', []));
        })
        .catch(e => {
          setIsLoading(false);
        });
    }
  }, [id]);
  return (
    <Card loading={isLoading} bodyStyle={{ padding: 0 }} title={t('facilities_recent')}>
      <section className="new new-widget-nobackground" id="news-list" style={{ paddingTop: 20, paddingBottom: 0 }}>
        <div className="container">
          <div className="row">
            {_.map(items, (item, index) => (
              <div key={index} className="col-md-12">
                <FacilityItem item={item} locale={setting.locale}></FacilityItem>
              </div>
            ))}
          </div>
        </div>
      </section>
    </Card>
  )
}
export default Page;
