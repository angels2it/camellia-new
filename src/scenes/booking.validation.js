import i18n from './../i18n'
const rules = {
  checkInDate: [
    {
      required: true,
      message: i18n.t('check_in_date_is_required'),
    },
  ],
  checkOutDate: [{ required: true, message: i18n.t('check_out_date_is_required') }],
  numofAdults: [{ required: true, message: i18n.t('num_of_adults_is_required') }],
  numOfChildrens: [{ required: true, message: i18n.t('num_of_childrens_is_required') }],
  qty: [{ required: true, message: i18n.t('booking_qty_is_required') }],
  guestName: [{ required: true, message: i18n.t('guest_name_is_required') }],
  guestPhone: [{ required: true, message: i18n.t('guest_phone_is_required') }],
  guestEmail: [{ required: true, type: 'email', message: i18n.t('guest_email_is_invalid') }],
};

export default rules;
