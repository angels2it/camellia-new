import React, { useContext, useEffect, useState } from "react";
import OwlCarousel from "react-owl-carousel";
import moment from "moment";
import _ from "lodash";
import { isMobile } from 'react-device-detect';
import Helmet from 'react-helmet';
import SettingContext from './../contexts/setting';

import Header from "./../components/header";
import Rooms from "./../components/rooms";
import News from "./../components/news";
import Banner from "./../components/banner";
import Footer from "./../components/footer";
import StretcherWrapper from "./../components/facilities";
import NearBy from "../components/nearby";
import About from "../components/about";
import resort from "../services/resort";
import FacilitiesMobile from "../components/facilities-mobile";

function Home({ changeLanguage }) {
  const setting = useContext(SettingContext);
  const [rooms, setRooms] = useState([]);
  useEffect(() => {
    resort
      .getRooms({
        checkInDate: moment().add(-1, "d").startOf("d").utc().format(),
        checkOutDate: moment().endOf("d").utc().format(),
        numOfChildrens: 1,
        numOfAdults: 1,
        currency: setting.locale === 'vi' ? "VND" : "USD"
      })
      .then((result) => {
        setRooms(_.get(result, "items", []))
      });
  }, []);
  return (
    <>
      <Helmet>
        <title>Camellia</title>
      </Helmet>
      <Header changeLanguage={changeLanguage} />
      <main>
        <Banner />
        <About />
        <Rooms items={rooms} />
        {!isMobile &&
          <StretcherWrapper />}
        {isMobile &&
          <FacilitiesMobile />}
        <News />
        <NearBy />
        {/* <ImageBlock /> */}
      </main>
      <Footer />
    </>
  );
}

const Subscribe = () => (
  <section className="subscribe">
    <div className="container">
      <div className="box">
        <h2 className="title">Subscribe</h2>
        <div className="text">
          <p>&amp; receive free premium gifts</p>
        </div>
        <div className="form-group">
          <input
            type="text"
            defaultValue
            placeholder="Subscribe"
            className="form-control"
          />
          <button className="btn btn-sm btn-main">Go</button>
        </div>
      </div>
    </div>
  </section>
);

const ImageBlock = () => (
  <section className="image-blocks image-blocks-header">
    <div
      className="section-header"
      style={{
        backgroundImage: `url(${require("./../assets/img/detail/header-1.jpg")})`,
      }}
    >
      <div className="container">
        <h2 className="title">
          <span>APARTMENT</span> RESIDENCES
          <a href="#" className="btn btn-sm btn-clean-dark">
            View more
          </a>
        </h2>
        <p>At home while on the road.</p>
      </div>
    </div>
    <div className="container">
      <div className="blocks_content">
        <div className="blocks">
          <div className="blocks_img">
            <img
              src={require("./../assets/img/detail/apartment-1.jpg")}
              alt=""
            />
          </div>
          <div className="blocks_text">
            <div className="text">
              {/* === room info === */}
              <h2 className="title">
                Presidential <br />
                Suite
              </h2>
              <p>
                Newly refurnished luxury accommodation with finest quality linen
                &amp; quality decor. Air Conditioning Electric Blankets, Ironing
                Boards, Mini Bar, Flat Screen &amp; Free Inhouse Movies, WiFi
                and Housekeeping by request.
              </p>
              {/* === room facilities === */}
              <div className="room-facilities">
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    {" "}
                    <i className="fas fa-universal-access" /> <br />4 Persons
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    <i className="fas fa-concierge-bell" /> <br /> Room service
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    {" "}
                    <i className="fas fa-bed" /> <br />
                    Kingsize bed
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    <i className="fas fa-utensils" /> <br /> All inclusive
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    <i className="far fa-clock" /> <br /> 460 sqft room
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    {" "}
                    <i className="fas fa-tv" />
                    <br /> TV
                  </figcaption>
                </figure>
              </div>
              {/* === booking === */}
              <div className="book">
                <div>
                  <a
                    href="room-overview.html"
                    className="btn btn-danger btn-lg"
                  >
                    Book
                  </a>
                </div>
                <div>
                  <span className="price h2">$ 129,00</span> <br />
                  <span>per night</span>
                </div>
              </div>
              {/*/booking*/}
            </div>
          </div>
        </div>
      </div>
      <div className="blocks_content">
        <div className="blocks">
          <div className="blocks_text">
            <div className="text">
              {/* === room info === */}
              <h2 className="title">
                Presidential <br />
                Suite
              </h2>
              <p>
                Newly refurnished luxury accommodation with finest quality linen
                &amp; quality decor. Air Conditioning Electric Blankets, Ironing
                Boards, Mini Bar, Flat Screen &amp; Free Inhouse Movies, WiFi
                and Housekeeping by request.
              </p>
              {/* === room facilities === */}
              <div className="room-facilities">
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    {" "}
                    <i className="fas fa-universal-access" /> <br />4 Persons
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    <i className="fas fa-concierge-bell" /> <br /> Room service
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    {" "}
                    <i className="fas fa-bed" /> <br />
                    Kingsize bed
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    <i className="fas fa-utensils" /> <br /> All inclusive
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    <i className="far fa-clock" /> <br /> 460 sqft room
                  </figcaption>
                </figure>
                {/*=== icon ===*/}
                <figure>
                  <figcaption>
                    {" "}
                    <i className="fas fa-tv" />
                    <br /> TV
                  </figcaption>
                </figure>
              </div>
              {/* === booking === */}
              <div className="book">
                <div>
                  <a
                    href="room-overview.html"
                    className="btn btn-danger btn-lg"
                  >
                    Book
                  </a>
                </div>
                <div>
                  <span className="price h2">$ 129,00</span> <br />
                  <span>per night</span>
                </div>
              </div>
              {/*/booking*/}
            </div>
          </div>
          <div className="blocks_img">
            <img
              src={require("./../assets/img/detail/apartment-1.jpg")}
              alt=""
            />
          </div>
        </div>
      </div>
    </div>
  </section>
);

const QuoteSlider = () => (
  <section
    className="quotes quotes-slider"
    style={{
      backgroundImage: `url(${require("./../assets/img/detail/header-1.jpg")})`,
    }}
  >
    <div className="section-header">
      <div className="container">
        <div className="text-center">
          <h2 className="title">TESTIMONIALS</h2>
          <p>What other think about us</p>
        </div>
      </div>
    </div>
    <div className="container">
      <div className="row justify-content-center">
        <div className="col-md-8">
          <OwlCarousel
            className="owl-quotes owl-theme"
            loop
            nav
            autoplay
            autoplayTimeout={4000}
            autoplayHoverPause
            dots
            margin={10}
            responsive={{
              0: {
                items: 1,
              },
              600: {
                items: 1,
              },
              1000: {
                items: 4,
              },
            }}
          >
            <div className="item">
              <div className="quote">
                <div className="text">
                  <h4>Glen Jordan</h4>
                  <p>
                    Ipsum dolore eros dolore <br />
                    dolor dolores sit iriure
                  </p>
                </div>
                <div className="more">
                  <div className="rating">
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                  </div>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="quote">
                <div className="text">
                  <h4>Glen Jordan</h4>
                  <p>
                    Ipsum dolore eros dolore <br />
                    dolor dolores sit iriure
                  </p>
                </div>
                <div className="more">
                  <div className="rating">
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                  </div>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="quote">
                <div className="text">
                  <h4>An An</h4>
                  <p>
                    Ipsum dolore eros dolore <br />
                    dolor dolores sit iriure
                  </p>
                </div>
                <div className="more">
                  <div className="rating">
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                  </div>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="quote">
                <div className="text">
                  <h4>Tran Nam</h4>
                  <p>
                    Ipsum dolore eros dolore <br />
                    dolor dolores sit iriure
                  </p>
                </div>
                <div className="more">
                  <div className="rating">
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                  </div>
                </div>
              </div>
            </div>
            <div className="item">
              <div className="quote">
                <div className="text">
                  <h4>Nguyen Anh</h4>
                  <p>
                    Ipsum dolore eros dolore <br />
                    dolor dolores sit iriure
                  </p>
                </div>
                <div className="more">
                  <div className="rating">
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                    <span className="fa fa-star" />
                  </div>
                </div>
              </div>
            </div>
          </OwlCarousel>
        </div>
      </div>
    </div>
  </section>
);

export default Home;
