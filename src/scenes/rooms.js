import React, { useEffect, useState, useContext } from "react";
import _ from "lodash";
import resortService from "./../services/resort";

import Header from "./../components/header";
import Banner from "./../components/banner";
import Footer from "./../components/footer";
import RoomItem from "../components/room-item";
import { useLocation } from "react-router-dom";
import qs from "query-string";
import SettingContext from "../contexts/setting";
import { Typography } from "antd";
import { useTranslation } from "react-i18next";
import moment from 'moment';
import Helmet from "react-helmet";

function Rooms({ changeLanguage, locale }) {
  const [items, setItems] = useState([]);
  const location = useLocation();
  const setting = useContext(SettingContext);
  // const [type, setType] = useState();
  const { t } = useTranslation();
  const filter = qs.parse(location.search) || {};

  useEffect(() => {
    filter.currency = setting.locale === 'vi' ? "VND" : "USD";
    filter.checkInDate = moment().startOf('d').utc().format();
    resortService.getRooms(filter).then((result) => {
      setItems(_.get(result, "items", []));
    });
  }, []);

  // useEffect(() => {
  //   if (filter.type) {
  //     const filterType = _.find(setting.roomTypes, t => t.id == filter.type);
  //     console.log(filterType)
  //     setType(filterType);
  //   }
  // }, [setting])
  return (
    <>
      <Header changeLanguage={changeLanguage} />
      <Helmet>
        <title>{t('rooms')}</title>
      </Helmet>
      <main>
        <Banner />
        <div className="image-blocks image-blocks-header">
          <div
            className="section-header"
            style={{
              backgroundImage: `url(${require("./../assets/img/detail/header-1.jpg")})`,
            }}
          >
            <div className="container">
              <p>{t('room.description')}</p>
            </div>
          </div>
        </div>
        <section className="rooms rooms-widget-nobackground" style={{ paddingTop: 20 }}>
          <div className="container">
            <div className="row">
              {_.map(items, (item, index) => (
                <div key={index} className="col-md-4">
                  <RoomItem item={item} locale={setting.locale}></RoomItem>
                </div>
              ))}
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
}

export default Rooms;
