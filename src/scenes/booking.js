import React, { useState, useContext, useMemo, forwardRef } from "react";
import OwlCarousel from "react-owl-carousel";
import { useHistory } from "react-router-dom";
import DatePicker from "react-datepicker";
import {
  Col,
  Form,
  Row,
  InputNumber,
  Button,
  Modal,
  Input,
  Steps,
  Card,
  Space,
  Descriptions,
  List,
  Select,
  Typography,
  Radio,
  Alert,
  Popover,
  Tooltip,
  Collapse,
  Divider
} from "antd";
import _ from "lodash";
import { useEffect } from "react";
import moment from "moment";
import qs from "query-string";
import { useLocation } from "react-router-dom";
import rules from "./booking.validation";
import Header from "../components/header";
import Footer from "../components/footer";
import { formatCurrency, countDiscount, formatRoomAttribute } from "./../helpers/utils";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import resortService from "./../services/resort";
import "./booking.scss";
import { useTranslation, Trans } from "react-i18next";
import Helmet from "react-helmet";
import SettingContext from "../contexts/setting";
import 'react-phone-input-2/lib/style.css'
import GuestInfoModal from './components/guestInfo';
import DateInput from './components/date-input';
import Attributes from './components/attributes';
import slugify from "slugify";

const Title = Typography.Title;
const { Panel } = Collapse;


const GuestPopup = ({ onChange }) => {
  const { t } = useTranslation();
  const [adults, setAdults] = useState(1);
  const [childrens, setChildrens] = useState(1);

  useEffect(() => {
    onChange && onChange({ numOfAdults: adults, numOfChildrens: childrens });
  }, [adults, childrens]);
  return (
    <List className="guest-popup">
      <List.Item
        actions={[
          <Button key="subtract" onClick={() => setAdults(adults - 1)}>
            -
          </Button>,
          <Button key="plus" onClick={() => setAdults(adults + 1)}>
            +
          </Button>,
        ]}
      >
        <List.Item.Meta
          avatar={<span className="people-number">{adults}</span>}
          title={t('adults')}
          description={t('adults_description')}
        />
      </List.Item>
      <List.Item
        actions={[
          <Button key="subtract" onClick={() => setChildrens(childrens - 1)}>
            -
          </Button>,
          <Button key="plus" onClick={() => setChildrens(childrens + 1)}>
            +
          </Button>,
        ]}
      >
        <List.Item.Meta
          avatar={<span className="people-number">{childrens}</span>}
          title={t('childrens')}
          description={t('childrens_description')}
        />
      </List.Item>
    </List>
  );
};

const RoomPopup = ({ onChange }) => {
  const { t } = useTranslation();
  const [rooms, setRooms] = useState([{ numOfAdults: 1, numOfChildrens: 1 }]);

  useEffect(() => {
    onChange && onChange(rooms);
  }, [rooms]);

  const onRoomChange = (index, data) => {
    rooms[index] = data;
    setRooms([...rooms]);
  }

  const addRoomHandler = () => {
    rooms.push({ numOfAdults: 1, numOfChildrens: 1 });
    setRooms([...rooms]);
  }

  const deleteRoomHandler = (index) => {
    rooms.splice(index, 1);
    setRooms([...rooms]);
  }

  return (
    <>
      {rooms.map((r, index) => <div className="filter-room-item" key={index}>
        <Typography.Title level={3}>{t('booking.roomQty')} {index + 1} {_.size(rooms) > 1 && <Button style={{ float: 'right' }} danger icon={<FontAwesomeIcon icon="times" />} onClick={() => deleteRoomHandler(index)} />}</Typography.Title>
        <GuestPopup key={index} onChange={data => onRoomChange(index, data)} />
      </div>)}
      <Button size="small" type="primary" onClick={addRoomHandler}>{t('booking.filter.add_room')}</Button>
    </>
  );
}

function QuickBooking({ handler }) {
  const { t } = useTranslation();
  const setting = useContext(SettingContext);
  const [checkInDate, setCheckInDate] = useState(
    moment().startOf("d").toDate()
  );
  const [checkOutDate, setCheckOutDate] = useState(
    moment().add(1, "d").endOf("d").toDate()
  );
  const [isGuestVisible, setIsGuestVisible] = useState(false);
  const [people, setPeople] = useState({ numOfAdults: 1, numOfChildrens: 1 });
  const [rooms, setRooms] = useState([{ numOfAdults: 1, numOfChildrens: 1 }]);
  const [roomInfo, setRoomInfo] = useState('');
  const history = useHistory();
  const location = useLocation();

  useEffect(() => {
    let filter = qs.parse(location.search);
    if (_.isEmpty(filter)) {
      filter = {
        checkInDate: moment().startOf("d").utc().format(),
        checkOutDate: moment().add(1, 'd').endOf("d").utc().format(),
        numOfAdults: 1,
        numOfChildrens: 1,
        qty: 1,
      };
    } else {
      filter.qty = 1;
    }
    const checkInDate = moment.utc(filter.checkInDate);
    const checkOutDate = moment.utc(filter.checkOutDate);
    setCheckInDate(checkInDate.toDate());
    setCheckOutDate(checkOutDate.toDate());
    const newFilters = {
      ...filter,
      checkInDate: checkInDate.format(),
      checkOutDate: checkOutDate.format(),
      currency: setting.locale === 'vi' ? "VND" : "USD",
      rooms
    }
    handler(newFilters)
  }, []);

  const roomChangeHandler = (value) => {
    setRooms(value);
  };

  useEffect(() => {
    setRoomInfo(`${_.size(rooms)} ${t('booking.roomQty')}, ${_.sumBy(rooms, 'numOfAdults')} ${t('adults')}, ${_.sumBy(rooms, 'numOfChildrens')} ${t('childrens')}`);
  }, [rooms])

  const booknowHandler = () => {
    const input = {
      checkInDate: moment(checkInDate).utc().format(),
      checkOutDate: moment(checkOutDate).utc().format(),
      ...people,
      rooms
    }
    handler(input);
  };
  return (
    <div className="banner__booking">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-md-3 col-6">
            <div className="book-date">
              <label>{t('arrival')} <i className="fas fa-long-arrow-alt-right" aria-hidden="true" /></label>
              <DatePicker
                selected={checkInDate}
                minDate={moment().toDate()}
                onChange={(date) => setCheckInDate(date)}
                customInput={<DateInput />}
              />
            </div>
          </div>
          <div className="col-md-3 col-6">
            <div className="book-date">
              <label>{t('departure')} <i className="fas fa-long-arrow-alt-right" aria-hidden="true" /></label>
              <input type="text" id="to-date" />
              <DatePicker
                selected={checkOutDate}
                minDate={checkInDate}
                onChange={(date) => setCheckOutDate(date)}
                customInput={<DateInput />}
              />
            </div>
          </div>

          <div className="col-md-3 col-12">
            <div className="book-date">
              <label className="guest">
                {t('booking.roomQty')}
              </label>
              <Popover
                content={<RoomPopup onChange={roomChangeHandler} />}
                title={t('guests_title')}
                trigger="click"
                visible={isGuestVisible}
                onVisibleChange={() => setIsGuestVisible(!isGuestVisible)}
              >
                <div className="date-value">
                  <span className="day" style={{ fontSize: 14 }}>
                    {roomInfo}
                  </span>
                </div>
              </Popover>
            </div>
          </div>
          <div className="col-md-2 col-12">
            <div className="book-now">
              <a
                onClick={booknowHandler}
                className="mt-4 button btn--big"
                id="bookNow"
              >
                <span>
                  {t('search')}
                  <small>{t('book_now_description')}</small>
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

const Banner = ({ okHandler }) => {
  const setting = useContext(SettingContext);
  var slides = useMemo(() => {
    return _.filter(_.get(setting, 'documents', []), d => d.type === 'SLIDE');
  }, [setting])
  return (
    <div className="banner small">
      {_.size(slides) > 0 && <OwlCarousel
        className="owl-theme"
        loop
        nav
        dots
        responsive={{
          0: {
            items: 1,
          },
          600: {
            items: 1,
          },
          1000: {
            items: 1,
          },
        }}
        id="sliderBanner"
      >
        {slides.map((s, index) => <div key={index} className="item">
          <div className="banner__item">
            <div className="banner__img">
              <img src={s.url} alt="" />
            </div>
            <div className="banner__content">
              <div className="container">
                <h1 className="title">
                  Camellia Tam Đảo
                <br />
                </h1>
              </div>
            </div>
          </div>
        </div>
        )}
      </OwlCarousel>}
      <QuickBooking handler={okHandler} />
    </div>
  )
};

function Booking({ changeLanguage }) {
  const [form] = Form.useForm();
  const [filters, setFilters] = useState({});
  const { t } = useTranslation();
  const setting = useContext(SettingContext);
  const [selectedRooms, setSelectedRooms] = React.useState([]);
  const [selectedRoomsPricing, setSelectedRoomsPricing] = React.useState(0);
  const [selectedRoomPricingItemIds, setSelectedRoomPricingItemIds] = React.useState([]);
  const [bookingVisible, setBookingVisible] = React.useState(false);

  const [rooms, setRooms] = useState([]);

  const booknowHandler = () => {
    setBookingVisible(true);
  };

  useEffect(() => {
    const ids = _.flatMap(selectedRooms, r => _.map(r.selectedPricingItems, 'id'));
    setSelectedRoomPricingItemIds(ids);
    let totalPrice = 0
    selectedRooms.forEach(room => {
      _.forEach(room.selectedPricingItems, item => {
        const discountValue = countDiscount(item.price, item.discount, item.discountType);
        item.totalDiscount = (item.price - discountValue) * item.bookingData.qty;
        item.bookingData.total = discountValue * item.bookingData.qty;
        totalPrice += (item.bookingData.total + item.bookingData.total * 10 / 100);
      });
    });
    setSelectedRoomsPricing(totalPrice);
  }, [selectedRooms]);

  const selectedRoomHandler = (room, priceItem) => {
    var currentSelectedRoom = _.find(selectedRooms, { id: room.id });
    const numOfRooms = _.size(selectedRoomPricingItemIds);
    if (currentSelectedRoom) {
      var currentPricingItem = _.find(currentSelectedRoom.selectedPricingItems, { id: priceItem.id })
      if (currentPricingItem) {
        if (currentPricingItem.bookingData.qty < (room.qty - room.usedQty)) {
          currentPricingItem.bookingData.qty++;
        }
      } else {
        currentSelectedRoom.selectedPricingItems.push({ ...priceItem, bookingData: { qty: 1, ..._.get(filters, `rooms[${numOfRooms}]`, _.get(filters, 'rooms[0]')) } });
      }
    } else {
      selectedRooms.push({ ...room, selectedPricingItems: [{ ...priceItem, bookingData: { qty: 1, ..._.get(filters, `rooms[${numOfRooms}]`, _.get(filters, 'rooms[0]')) } }] });
    }
    setSelectedRooms([...selectedRooms]);
  }

  const removePricingItemHandler = (roomId, pricingItemId) => {
    var currentSelectedRoom = _.find(selectedRooms, { id: roomId });
    if (currentSelectedRoom) {
      var currentPricingItemIndex = _.findIndex(currentSelectedRoom.selectedPricingItems, { id: pricingItemId });
      if (currentPricingItemIndex >= 0) {
        currentSelectedRoom.selectedPricingItems.splice(currentPricingItemIndex, 1);
        if (_.size(currentSelectedRoom.selectedPricingItems) === 0) {
          var roomIndex = _.findIndex(selectedRooms, { id: roomId });
          selectedRooms.splice(roomIndex, 1);
        }
      }
    }
    setSelectedRooms(_.isEmpty(selectedRooms) ? [] : [...selectedRooms]);
  }

  const searchHandler = (values) => {
    const newFilters = {
      ...values,
      currency: setting.locale === 'vi' ? "VND" : "USD",
    };
    setFilters(newFilters);
    setSelectedRooms([]);
    resortService.getRooms(newFilters).then((data) => {
      setRooms(_.get(data, "items", []));
    });;
  };

  const cancelHandler = (reset = true) => {
    setBookingVisible(false);
    if (reset) {
      setSelectedRooms([]);
      searchHandler(filters);
    }
  }

  return (
    <>
      <Helmet>
        <title>{t('booking.title')}</title>
      </Helmet>
      <Header changeLanguage={changeLanguage} />
      <main id="booking">
        <Banner okHandler={searchHandler} />
        <br />
        <Row gutter={[10, 10]}>
          <Col md={{ span: 12, offset: 3 }} xs={{ span: 18, offset: 3 }}>
            <Title level={3}>{t('booking.please_select_room')}</Title>
            <span style={{ fontFamily: 'sans-serif', paddingBottom: 10, display: 'block' }} level={5}><Trans components={{ strong: <strong /> }}>{t('booking.please_select_room_description')}</Trans></span>
            <ListRooms locale={setting.locale} items={rooms} onBookNow={selectedRoomHandler} selectedRooms={selectedRooms} selectedRoomPricingItemIds={selectedRoomPricingItemIds} />
          </Col>
          <Col md={{ span: 6 }} xs={{ span: 18, offset: 3 }} className="no-xs-offset">
            <Card title={<>
              <Typography.Title level={4}>{t('booking.selected_rooms')}{_.size(selectedRooms) > 0 && <span style={{ float: 'right' }}>{formatCurrency(selectedRoomsPricing, _.get(selectedRooms, 'currency', 'VND'))}</span>}</Typography.Title>
              <Descriptions column={2} colon={false} layout="vertical" style={{ marginTop: 10 }}>
                <Descriptions.Item label={t('resort.checkin_from')}>{setting['App.UI.CheckInFrom']}</Descriptions.Item>
                <Descriptions.Item label={t('resort.checkout_before')}>{setting['App.UI.CheckOutBefore']}</Descriptions.Item>
              </Descriptions>
            </>} className="search-room-selected">
              <List>
                {_.map(_.flatMap(selectedRooms, r => _.map(r.selectedPricingItems, p => ({ r, p }))), ({ r, p }, index) =>
                (<div key={index}>
                  <Typography.Title level={3} className="search-room-selected-index">
                    {`${t('booking.roomQty')} ${index + 1}`}
                    <span className="search-room-selected-price">
                      {formatCurrency(countDiscount(p.price, p.discount, p.discountType), r.currency)}
                    </span>
                  </Typography.Title>
                  <br />
                  <Typography.Title level={5} className="search-room-selected-index">
                    {`${t('booking.payment.tax')}`}
                    <span className="search-room-selected-price">
                      {formatCurrency(countDiscount(p.price, p.discount, p.discountType) * 10 / 100, r.currency)}
                    </span>
                  </Typography.Title>

                  <List.Item key={p.id} title={r.name} actions={[<Button onClick={() => removePricingItemHandler(r.id, p.id)} danger icon={<FontAwesomeIcon icon="trash" key="list-delete"></FontAwesomeIcon>}></Button>]}>
                    <List.Item.Meta
                      title={r.name}
                      description={<>
                        {p.name}
                        <br />
                        {(t('room.max_people_value', { adults: p.bookingData.numOfAdults, childrens: p.bookingData.numOfChildrens }))}
                      </>}
                    />
                  </List.Item>
                </div>)
                )}
              </List>
              {_.isEmpty(selectedRooms) && <p>{t('booking.no_select')}</p>}
              {_.size(selectedRooms) > 0 && <Button type="primary" onClick={booknowHandler}>{t('book_now')}</Button>}
            </Card>
          </Col>
        </Row>
        <GuestInfoModal
          visible={bookingVisible}
          selectedRooms={selectedRooms}
          initialValues={filters}
          onCancel={cancelHandler}
        />
      </main>
      <Footer />
    </>
  );
}

const ListRooms = ({ items, onBookNow, selectedRooms, selectedRoomPricingItemIds, locale }) => {
  return (
    items.map((item) => (
      <RoomItem locale={locale} key={item.id} onBookNow={onBookNow} item={item} selectedRooms={selectedRooms} selectedRoomPricingItemIds={selectedRoomPricingItemIds} />
    ))
  );
};

const PricingItem = ({
  room,
  selectedRoomPricingItemIds,
  selectedRoom,
  price,
  discount,
  discountType,
  index,
  onClick,
  ...pricing
}) => {
  const {
    currency,
    qty,
    usedQty,
  } = room;
  const [expandedAttribute, setExpandedAttribute] = useState();
  const { t } = useTranslation();

  const expandAttributeHandler = (a) => {
    if (expandedAttribute && expandedAttribute.id == a.attribute.id) {
      setExpandedAttribute(null);
    } else {
      setExpandedAttribute(a.attribute);
    }
  }
  return (
    <List.Item key={index}>
      <Collapse activeKey={[pricing.id]} bordered={false} ghost expandIcon={() => null} className="attribute-collapse" style={{ width: '100%' }}>
        <Panel key={pricing.id} header={
          <Row style={{ width: '100%' }}>
            <Col md={{ span: 18 }} xs={{ span: 24 }}>
              <Typography.Title level={4}>{pricing.name}</Typography.Title>
              <Attributes items={pricing.attributes} onAttributeClick={expandAttributeHandler} />
            </Col>
            <Col md={{ span: 6 }} xs={{ span: 24 }} style={{ textAlign: 'right' }}>
              <div style={{ fontSize: '30dp', display: "block" }}>
                <span
                  className={
                    discount > 0 ? "room-price room-price-discount" : "room-price"
                  }
                >
                  {formatCurrency(price, currency)}
                </span>
                {discount > 0 && (
                  <span className="room-discount">
                    {formatCurrency(
                      countDiscount(price, discount, discountType),
                      currency
                    )}
                  </span>
                )}
              </div>
              {_.includes(selectedRoomPricingItemIds, pricing.id) ?
                <Button type="primary" icon={<FontAwesomeIcon icon="check" style={{ marginRight: 5 }} />} style={{ marginTop: 10 }}>
                  {t('booking.selected')}
                </Button> :
                <Button type="primary" disabled={qty <= (usedQty + (selectedRoom ? _.sumBy(selectedRoom.selectedPricingItems, r => r.bookingData.qty) : 0))} onClick={() => onClick(index)} style={{ marginTop: 10 }}>
                  {t('booking.select')}
                </Button>
              }
            </Col>
          </Row>
        }>
          {expandedAttribute &&
            <Card type="inner">
              <Card.Meta title={expandedAttribute.name} description={expandedAttribute.description}></Card.Meta>
              {expandedAttribute.link && <Row>
                <Col span={24} style={{ textAlign: 'right' }}>
                  <Button href={expandedAttribute.link} type="link" style={{ padding: 0 }}>{t('policy')}</Button>
                </Col>
              </Row>}
            </Card>}
        </Panel>
      </Collapse>
    </List.Item>
  )
}

const RoomItem = ({ item, onBookNow, selectedRooms, selectedRoomPricingItemIds, locale }) => {
  const { t } = useTranslation();

  const {
    id,
    name,
    bedType,
    thumbnail,
    currency,
    qty,
    usedQty,
    description,
    pricingItems = [],
    albums = []
  } = item;
  const onClick = (index) => {
    onBookNow && onBookNow(item, pricingItems[index]);
  };
  const slides = _.concat([{ url: thumbnail }], albums || []);
  const selectedRoom = useMemo(() => _.find(selectedRooms, r => r.id == id), [selectedRooms]);
  return (
    <Row gutter={[20, 20]} key={id}>
      <Col span={24}>
        <Card title={<Title level={4}>{name}<a className="viewmore" target="_blank" href={`/${locale}/rooms/${id}/${slugify(name)}`}>{t('room.detail')}</a></Title>} cover={
          _.size(slides) > 0 && <OwlCarousel
            className="owl-theme"
            loop
            nav
            style={{ height: 400 }}
            dots
            responsive={{
              0: {
                items: 1,
              },
              600: {
                items: 1,
              },
              1000: {
                items: 1,
              },
            }}
            id="sliderBanner"
          >
            {slides.map((s, index) => <div key={index} className="item">
              <div className="banner__item">
                <div className="banner__img">
                  <img style={{ height: '400px' }} className="growimg" src={s.url} alt="" />
                </div>
                <div className="banner__content">
                  <div className="container">
                  </div>
                </div>
              </div>
            </div>
            )}
          </OwlCarousel>
        } className={"search-room-result grow"}>
          <Row align="stretch" justify="space-between">
            <Col span={18}>
              <p>{bedType.name}</p>
              <Row gutter={[10, 10]}>
                <Col span={24}>{description && <p>{description}</p>}</Col>
              </Row>
            </Col>
            <Col span={6} style={{ textAlign: 'right' }}>
              <p>{qty - usedQty}{" "} {t('available')}</p>
            </Col>
          </Row>
          <List>
            {pricingItems && pricingItems.map((p, index) =>
              <PricingItem key={index} {...p} room={item} index={index} selectedRoom={selectedRoom} selectedRoomPricingItemIds={selectedRoomPricingItemIds} onClick={onClick} />)}
          </List>
        </Card>
      </Col>
    </Row>
  );
};

export default Booking;
