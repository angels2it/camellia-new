import React, { useEffect, useState, useContext } from "react";
import OwlCarousel from "react-owl-carousel";
import moment from "moment";
import _ from "lodash";
import resortService from "./../services/resort";

import Header from "./../components/header";
import Banner from "./../components/banner";
import Footer from "./../components/footer";
import NewsItems from "../components/news-item";
import Helmet from "react-helmet";
import { useTranslation } from "react-i18next";
import SettingContext from "../contexts/setting";

function News({ changeLanguage }) {
  const [items, setItems] = useState([]);
  const { t } = useTranslation();
  const settings = useContext(SettingContext);
  useEffect(() => {
    resortService.getNews().then((result) => {
      setItems(_.get(result, "items", []));
    });
  }, []);
  return (
    <>
      <Header changeLanguage={changeLanguage} />
      <Helmet>
        <title>{t('news')}</title>
      </Helmet>
      <main>
        <Banner />
        <section className="new new-widget-nobackground" id="news-list">
          <div className="container">
            <div className="row">
              {_.map(items, (item, index) => (
                <div key={index} className="col-md-4">
                  <NewsItems item={item} locale={settings.locale}></NewsItems>
                </div>
              ))}
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
}

export default News;
