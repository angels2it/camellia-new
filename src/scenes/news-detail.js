import { Card, Col, PageHeader, Row, Typography } from "antd";
import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import _ from "lodash";

import resort from "../services/resort";

const { Title } = Typography;

const Page = () => {
  const [item, setItem] = useState({});
  const { thumbnail = "", title = "", content = "", creatorUser = {} } = item;
  let { slug } = useParams();

  useEffect(() => {
    if (!_.isEmpty(slug)) {
      resort.getBySlug(slug).then((data) => {
        setItem(data);
      });
    }
  }, [slug]);

  return (
    <div id="page">
      <Row>
        <Col offset={2} span={20}>
          <p>{creatorUser.name}</p>
        </Col>
      </Row>
      <Row style={{ marginTop: 10 }}>
        <Col offset={2} md={{ span: 16 }} xs={{ span: 24 }}>
          <div
            className="content__editor"
            dangerouslySetInnerHTML={{ __html: content }}
          />
        </Col>
        <Col md={{ span: 6 }} xs={{ span: 24 }}>
          {item && <RecentNews id={item.id} />}
        </Col>
      </Row>
    </div>
  );
};


const RecentNews = ({ id }) => {
  const [news, setNews] = useState([]);
  const [isLoading, setIsLoading] = useState(false);
  useEffect(() => {
    if (id > 0) {
      setIsLoading(true);
      resortService.getNews({ exceptId: id, maxResultCount: 5 })
        .then(r => {
          setIsLoading(false);
          setNews(_.get(r, 'items', []));
        })
        .catch(e => {
          setIsLoading(false);
        });
    }
  }, [id]);
  return (
    <Card loading={isLoading}>

    </Card>
  )
}

export default Page;
