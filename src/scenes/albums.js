import React, { createRef, useEffect, useState } from "react";
import _ from "lodash";
import ImageGallery from "react-image-gallery";
import { Row, Col, Radio } from 'antd';
import "react-image-gallery/styles/css/image-gallery.css";
import resort from "../services/resort";
import Header from './../components/header';
import Footer from './../components/footer';
import Helmet from "react-helmet";
import { useTranslation } from "react-i18next";

const Albums = ({ changeLanguage }) => {
  const [items, setItems] = useState([]);
  const [types, setTypes] = useState([]);
  const [type, setType] = useState('ALBUM');
  const ref = createRef();
  const { t } = useTranslation();
  useEffect(() => {
    resort.getAlbumsTypes().then(data => {
      setTypes(data);
    })
  }, []);

  useEffect(() => {
    if (type === 'ALBUM') {
      resort.getAlbums().then(data => {
        setItems(data || []);
      });
    } else {
      const selectedType = _.find(types, t => t.refId === type);
      console.log(selectedType, type)
      if (selectedType) {
        resort.getAlbums(selectedType.type, selectedType.refId, selectedType.refType).then(data => {
          setItems(data);
        })
      }
    }
  }, [type])

  useEffect(() => {
    ref.current && ref.current.process();
  }, [items]);

  const photos = _.map(items, (item, index) => ({
    original: item.url,
    alt: item.name,
    thumbnail: item.url,
  }));

  const handleTypeChange = (e) => {
    setType(e.target.value);
  }
  if (_.isEmpty(photos)) {
    return null;
  }
  return (
    <>
      <Header defaultSticky={true} changeLanguage={changeLanguage} />
      <Helmet>
        <title>{t('albums')}</title>
      </Helmet>
      <main id="booking">
        <Row align="middle">
          <Col span={24} style={{ textAlign: 'center' }}>
            <Radio.Group defaultValue={type} buttonStyle="solid album-button" onChange={handleTypeChange}>
              <Radio.Button key="ALBUM" value="ALBUM">{t('albums')}</Radio.Button>
              {types.map(t => <Radio.Button key={t.refId} value={t.refId}>{t.name}</Radio.Button>)}
            </Radio.Group>
          </Col>
        </Row>
        <ImageGallery items={photos} />
      </main>
      <Footer />
    </>
  );
};

export default Albums;
