import React, { useContext, useEffect, useState } from "react";
import OwlCarousel from "react-owl-carousel";
import moment from "moment";
import _ from "lodash";
import resortService from "./../services/resort";

import Header from "./../components/header";
import Banner from "./../components/banner";
import Footer from "./../components/footer";
import ServiceItem from "./../components/service-item";
import SettingContext from "../contexts/setting";

function Services({ changeLanguage }) {
  const [items, setItems] = useState([]);
  const setting = useContext(SettingContext);
  useEffect(() => {
    resortService.getFacilities("SERVICE", 100).then((result) => {
      setItems(_.get(result, "items", []));
    });
  }, []);
  return (
    <>
      <Header changeLanguage={changeLanguage} />
      <main>
        <Banner />
        <section className="cards">
          <div className="container">
            <div className="row">
              {_.map(items, (item, index) => (
                <div
                  key={index}
                  className={
                    index === 0 ? "col-xs-12 col-md-8" : "col-xs-6 col-md-4"
                  }
                >
                  <ServiceItem item={item} locale={setting.locale}></ServiceItem>
                </div>
              ))}
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
}

export default Services;
