import React, { forwardRef } from 'react';
import moment from 'moment';

const DateInput = forwardRef(({ value, onClick }, ref) => {
    const date = moment(value, "MM/DD/YYYY");
    return (
        <>
            <div className="date-value" onClick={onClick}>
                <span className="day">{date.format("DD")}</span>
                <span className="month">{date.format("MMM")}</span>
                <span className="year">{date.format("YYYY")}</span>
            </div>
        </>
    );
});

export default DateInput;