import React, { useState, useContext, useMemo, forwardRef } from "react";
import OwlCarousel from "react-owl-carousel";
import { Link, useHistory } from "react-router-dom";
import DatePicker from "react-datepicker";
import {
    Col,
    Form,
    Row,
    InputNumber,
    Button,
    Modal,
    Input,
    Steps,
    Card,
    Space,
    Descriptions,
    List,
    Select,
    Typography,
    Radio,
    Alert,
    Popover,
    Tooltip,
    Checkbox
} from "antd";
import _ from "lodash";
import { useEffect } from "react";
import moment from "moment";
import rules from "./../booking.validation";
import { formatCurrency, countDiscount } from "./../../helpers/utils";
import resortService from "./../../services/resort";
import "./../booking.scss";
import { useTranslation, Trans } from "react-i18next";
import ReactFlagsSelect from 'react-flags-select';
import SettingContext from "../../contexts/setting";
import PhoneInput from 'react-phone-input-2'
import 'react-phone-input-2/lib/style.css'
import countries from './../../helpers/countries'
import Attributes from "./attributes";

const Title = Typography.Title;

const GuestInfoModal = ({ visible, onCancel, selectedRooms, initialValues }) => {
    const [form] = Form.useForm();
    const { t } = useTranslation();
    const [currentStep, setCurrentStep] = useState(1)
    const [rooms, setRooms] = useState(selectedRooms)
    const [roomPrice, setRoomPrice] = useState({});
    const [price, setPrice] = useState(0);
    const [priceWithVat, setPriceWithVat] = useState(0);
    const [bookingData, setBookingData] = useState({});
    const [countryCode, setCountryCode] = useState();
    const [isConfirmed, setIsConfirmed] = useState(false);
    const setting = useContext(SettingContext);

    useEffect(() => {
        if (!visible) {
            form.resetFields();
            setCurrentStep(1);
            setBookingData({});
        } else {
            let roomIndex = 1;
            setRooms(_.map(selectedRooms, r => {
                _.forEach(r.selectedPricingItems, (p) => {
                    p.index = roomIndex;
                    roomIndex++;
                });
                return r;
            }))
        }
    }, [visible, selectedRooms]);

    useEffect(() => {
        form.setFieldsValue({ guestCountry: countries[countryCode] })
    }, [countryCode]);

    useEffect(() => {
        if (!_.isEmpty(initialValues)) {
            form.setFieldsValue({ ...initialValues, rooms });
        }
    }, [initialValues, rooms])

    useEffect(() => {
        handleQtyChange();
    }, [rooms]);

    const handleIsConfirmedChange = () => {
        setIsConfirmed(!isConfirmed);
    }

    const cancelHandler = () => {
        onCancel(false);
    }

    const okHandler = () => {
        if (currentStep === 2) {
            onCancel();
            return;
        }
        form.validateFields().then(async (values) => {
            try {
                var bookingInput = {
                    ..._.omit(values, ['rooms']),
                    checkInDate: initialValues.checkInDate,
                    checkOutDate: initialValues.checkOutDate,
                    currency: _.get(values, 'rooms.[0].currency'),
                    total: price,
                    totalWithVat: priceWithVat,
                    vat: 10
                }
                bookingInput.rooms = values.rooms.map(r => {
                    return {
                        id: r.id,
                        price: roomPrice[r.id],
                        pricingItems: r.selectedPricingItems.map(p => {
                            const discountValue = countDiscount(p.price, p.discount, p.discountType);
                            const totalDiscount = (p.price - discountValue) * p.bookingData.qty;
                            const total = discountValue * p.bookingData.qty;
                            return {
                                id: p.id,
                                price: p.price,
                                discount: p.discount,
                                discountType: p.discountType,
                                ...p.bookingData,
                                totalDiscount,
                                total
                            }
                        })
                    };
                })
                const result = await resortService.booking(bookingInput);
                Modal.success({ title: t("success"), content: t("booking_success") });
                setBookingData({ code: result, ...values, success: true });
                setCurrentStep(2);
            } catch (error) {
                console.log(error)
                Modal.error({ title: t("error"), content: t("booking_error") });
            }
        });
    };
    const handleQtyChange = () => {
        if (_.size(rooms) > 0) {
            let totalPrice = 0
            rooms.forEach(room => {
                roomPrice[room.id] = 0;
                _.forEach(room.selectedPricingItems, item => {
                    const discountValue = countDiscount(item.price, item.discount, item.discountType);
                    item.totalDiscount = (item.price - discountValue) * item.bookingData.qty;
                    item.bookingData.total = discountValue * item.bookingData.qty;
                    roomPrice[room.id] += item.bookingData.total;
                    totalPrice += item.bookingData.total;
                })
            });
            setRoomPrice({ ...roomPrice });
            setPrice(totalPrice);
            setPriceWithVat(totalPrice + (totalPrice * 10 / 100));
        }
    }
    const handleFormValueChange = (changedValues, values) => {
        if (_.has(changedValues, 'rooms')) {
            setRooms(_.cloneDeep(values.rooms));
        }
    }
    return (
        <Modal
            visible={visible}
            forceRender={true}
            width={1000}
            onCancel={cancelHandler}
            closeIcon={<></>}
            bodyStyle={{ padding: 5 }}
            footer={
                <Card bordered={false} size="small">
                    <Row style={{ textAlign: 'left' }}>
                        <Col span={24}>
                            <Title level={5}>
                                {t('booking.resortNote1')}
                                {t('booking.resortNote2')}
                            </Title>
                        </Col>
                        <Col span={24}>
                            <Title level={5}>
                                <Checkbox value={isConfirmed} onChange={handleIsConfirmedChange}></Checkbox> <Trans t={t} components={{ Link: <Link /> }}>{t('booking.policyNote')}</Trans>
                            </Title>
                        </Col>
                    </Row>
                    {currentStep == 1 && <Row style={{ textAlign: 'center' }} justify="center" gutter={[20, 20]}>
                        <Col md={{ span: 4 }} xs={{ span: 12 }}>
                            <Button disabled={!isConfirmed} onClick={okHandler} style={{ height: '50px' }} block type="primary">{t('booking.confirm')}</Button>
                        </Col>
                        <Col md={{ span: 4 }} xs={{ span: 12 }}>
                            <Button onClick={cancelHandler} style={{ height: '50px' }} block type="primary" danger>{t('cancel')}</Button>
                        </Col>
                    </Row>}
                    {currentStep == 2 && <Row style={{ textAlign: 'center' }} justify="center" gutter={[20, 20]}>
                        <Col md={{ span: 4 }} xs={{ span: 12 }}>
                            <Button onClick={okHandler} style={{ height: '50px' }} block type="primary">{t('OK')}</Button>
                        </Col>
                    </Row>}
                </Card>
            }
        >
            <Form form={form} layout="vertical" initialValues={{ guestTitle: 'MR' }} onValuesChange={handleFormValueChange} style={{ lineHeight: 0 }}>
                <Space direction="vertical" size={5} style={{ width: '100%' }}>
                    {bookingData.success &&
                        <>
                            <Alert style={{ marginTop: 30 }} message={`${t('booking.code')} ${bookingData.code}`} description={t("booking_success")} />
                        </>
                    }
                    <Card title={t('booking.step_0')} style={{ width: '100%' }} type="inner" size="small">
                        <Descriptions title={setting['App.UI.Name']}>
                            <Descriptions.Item label={t('resort.phone')}>{setting['App.UI.Phone']}</Descriptions.Item>
                            <Descriptions.Item label={t('resort.email')}>{setting['App.UI.Email']}</Descriptions.Item>
                            <Descriptions.Item label={t('resort.address')}>{setting['App.UI.Address']}</Descriptions.Item>
                            <Descriptions.Item label={t('resort.checkin_from')}>{setting['App.UI.CheckInFrom']}</Descriptions.Item>
                            <Descriptions.Item label={t('resort.checkout_before')}>{setting['App.UI.CheckOutBefore']}</Descriptions.Item>
                        </Descriptions>
                    </Card>

                    <Card title={t('booking.step_1')} style={{ width: '100%' }} type="inner" size="small">
                        {currentStep === 2 &&
                            <Descriptions>
                                <Descriptions.Item label={t('arrival')}>{moment.utc(initialValues.checkInDate).local().format('DD-MM-YYYY')}</Descriptions.Item>
                                <Descriptions.Item label={t('departure')}>{moment.utc(initialValues.checkOutDate).local().format('DD-MM-YYYY')}</Descriptions.Item>
                                <Descriptions.Item label={t('sumOfDays')}>{Math.ceil(moment.duration(moment(initialValues.checkOutDate).diff(moment(initialValues.checkInDate))).as('d')) - 1}</Descriptions.Item>
                            </Descriptions>}
                        <Form.List name="rooms">
                            {fields => (
                                <div>
                                    {fields.map(field => {
                                        const room = rooms[field.key];
                                        if (_.isEmpty(room)) {
                                            return null;
                                        }
                                        return (
                                            <div key={field.key} className="booking-data-room-item">
                                                <Typography.Title level={3}>{room.name}</Typography.Title>
                                                <Descriptions>
                                                    <Descriptions.Item label={t('room.bed_type')}>{room.bedType.name}</Descriptions.Item>
                                                    <Descriptions.Item label={t('room.num_of_beds')}>{room.numOfBeds}</Descriptions.Item>
                                                    <Descriptions.Item label={t('room.max_people')}>{t('room.max_people_value', { adults: room.maxAdults, childrens: room.maxChildrens })}</Descriptions.Item>
                                                </Descriptions>
                                                {currentStep === 2 &&
                                                    <>
                                                        <Descriptions>
                                                            <Descriptions.Item label={t('arrival')}>{moment.utc(initialValues.checkInDate).local().format('DD-MM-YYYY')}</Descriptions.Item>
                                                            <Descriptions.Item label={t('departure')}>{moment.utc(initialValues.checkOutDate).local().format('DD-MM-YYYY')}</Descriptions.Item>
                                                        </Descriptions>
                                                        <br />
                                                        <Typography.Title level={4}>{t('booking.step_2')}</Typography.Title>
                                                        <Attributes items={room.attributes} />
                                                        <br />
                                                        {room.selectedPricingItems.map(item => {
                                                            return (<Card key={item.id} title={item.name} className="booking-data-room-item-price">
                                                                <Descriptions>
                                                                    <Descriptions.Item label={t('booking.qty')}>{item.bookingData.qty}</Descriptions.Item>
                                                                    <Descriptions.Item label={t('adults')}>{item.bookingData.numOfAdults}</Descriptions.Item>
                                                                    <Descriptions.Item label={t('childrens')}>{item.bookingData.numOfChildrens}</Descriptions.Item>
                                                                </Descriptions>
                                                                <Row>
                                                                    <Col span={24}>
                                                                        <Typography.Title level={4}>{t('total_price')}: {formatCurrency(countDiscount(item.price, item.discount, item.discountType) * item.bookingData.qty, room.currency)}</Typography.Title>
                                                                    </Col>
                                                                </Row>
                                                            </Card>)
                                                        })}
                                                    </>}
                                                {currentStep === 1 &&
                                                    <>
                                                        {initialValues.checkInDate && initialValues.checkOutDate && <Row>
                                                            <Descriptions>
                                                                <Descriptions.Item label={t('arrival')}>{moment.utc(initialValues.checkInDate).local().format('DD-MM-YYYY')}</Descriptions.Item>
                                                                <Descriptions.Item label={t('departure')}>{moment.utc(initialValues.checkOutDate).local().format('DD-MM-YYYY')}</Descriptions.Item>
                                                                <Descriptions.Item label={t('sumOfDays')}>{Math.ceil(moment.duration(moment(initialValues.checkOutDate).diff(moment(initialValues.checkInDate))).as('d')) - 1}</Descriptions.Item>
                                                            </Descriptions>
                                                        </Row>}
                                                        <Typography.Title level={4}>{t('booking.step_2')}</Typography.Title>
                                                        <Attributes items={room.attributes} />
                                                        <Form.List name={[field.key, 'selectedPricingItems']}>
                                                            {pricingItemfields => (
                                                                <div>
                                                                    {pricingItemfields.map(pricingItemField => {
                                                                        const item = rooms[field.key].selectedPricingItems[pricingItemField.key];
                                                                        const itemAvailableQty = room.qty - room.usedQty - _.sumBy(rooms[field.key].selectedPricingItems, p => p.id === item.id ? 0 : p.bookingData.qty);
                                                                        return (
                                                                            <Card key={pricingItemField.key} title={`${t('booking.roomQty')} ${item.index} - ${item.name}`} className="booking-data-room-item-price" size="small">
                                                                                <Typography.Title level={4}>{t('booking.attributes')}</Typography.Title>
                                                                                <Attributes items={item.attributes} />
                                                                                <Typography.Title level={4}>{t('booking.step_4')}</Typography.Title>
                                                                                <Row>
                                                                                    <Col span={8}>
                                                                                        <Form.Item label={t('booking.qty')} name={[pricingItemField.key, 'bookingData', 'qty']}>
                                                                                            <Select onChange={handleQtyChange} style={{ width: '90%' }}>
                                                                                                {_.times(itemAvailableQty, i => <Select.Option key={i} value={i + 1}>{(i + 1)} {t('booking.roomQty')}</Select.Option>)}
                                                                                            </Select>
                                                                                        </Form.Item>
                                                                                    </Col>

                                                                                    <Col span={8}>
                                                                                        <Form.Item label={t('adults')} name={[pricingItemField.key, 'bookingData', 'numOfAdults']}>
                                                                                            <InputNumber min={1} />
                                                                                        </Form.Item>
                                                                                    </Col>

                                                                                    <Col span={8}>
                                                                                        <Form.Item label={t('childrens')} name={[pricingItemField.key, 'bookingData', 'numOfChildrens']}>
                                                                                            <InputNumber min={0} />
                                                                                        </Form.Item>
                                                                                    </Col>
                                                                                </Row>
                                                                                <Row>
                                                                                    <Col span={24}>
                                                                                        <Typography.Title level={4}>{t('total_price')}: {formatCurrency(countDiscount(item.price, item.discount, item.discountType) * item.bookingData.qty, room.currency)}</Typography.Title>
                                                                                    </Col>
                                                                                </Row>
                                                                            </Card>
                                                                        )
                                                                    })}
                                                                </div>
                                                            )}
                                                        </Form.List>
                                                    </>}
                                            </div>
                                        )
                                    })}
                                </div>
                            )}
                        </Form.List>
                    </Card>

                    <Card title={t('booking.step_5')} style={{ width: '100%' }} type="inner" size="small">
                        {rooms.map(room => {
                            return (
                                <Row key={room.id}>
                                    <Col span={12}>
                                        <Typography.Title level={4}>{room.name}</Typography.Title>
                                    </Col>
                                    <Col span={8}>
                                        <Typography.Title level={4} className="booking-price-summary">
                                            {formatCurrency(roomPrice[room.id] || 0, room.currency)}
                                        </Typography.Title>
                                    </Col>
                                </Row>
                            )
                        })}
                        {_.size(rooms) > 0 && (
                            <>
                                <Row>
                                    <Col span={12}>
                                        <Typography.Title level={4}>{t('tax')}</Typography.Title>
                                    </Col>
                                    <Col span={8}>
                                        <Typography.Title className="booking-price-summary" level={4}>{formatCurrency(price * 10 / 100, _.get(rooms, '[0].currency'))}</Typography.Title>
                                    </Col>
                                </Row>
                                <Row>
                                    <Col span={12}>
                                        <Typography.Title level={4}>{t('total')}</Typography.Title>
                                    </Col>
                                    <Col span={8}>
                                        <Typography.Title className="booking-price-summary" level={4}>{formatCurrency(priceWithVat, _.get(rooms, '[0].currency'))}</Typography.Title>
                                    </Col>
                                </Row>
                            </>)}
                    </Card>

                    <Card title={t('booking.step_3')} style={{ width: '100%' }} type="inner" size="small">
                        {currentStep === 1 && <Form.Item name="note" label={t('booking.note')}>
                            <Input.TextArea rows={4} />
                        </Form.Item>}
                        {currentStep === 2 && <p>{bookingData.note}</p>}
                    </Card>

                    {currentStep === 1 && <Card title={t('booking.step_4')} style={{ width: '100%' }} type="inner" size="small">
                        <Row gutter={[5, 5]}>
                            <Col md={{ span: 12 }} xs={{ span: 24 }}>
                                <Title level={3}>{t('booking.guest')}</Title>
                                <Row gutter={[5, 5]}>
                                    <Col span={24}>
                                        <Form.Item
                                            name="guestEmail"
                                            rules={rules.guestEmail}
                                            label={t('guest_email')}
                                        >
                                            <Input />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row gutter={[5, 5]}>
                                    <Col md={{ span: 24 }} xs={{ span: 24 }}>
                                        <Form.Item
                                            name="guestPhone"
                                            rules={rules.guestPhone}
                                            label={t('guest_phone')}
                                        >
                                            <PhoneInput
                                                inputStyle={{ height: 30 }}
                                                country={'vn'}
                                            />
                                        </Form.Item>
                                    </Col>

                                </Row>
                                <Row gutter={[5, 5]}>
                                    <Col md={{ span: 24 }} xs={{ span: 24 }}>
                                        <Form.Item name="guestTitle" label={t('guest_title')}>
                                            <Radio.Group>
                                                <Radio.Button value="MR">{t('mr')}</Radio.Button>
                                                <Radio.Button value="MS">{t('ms')}</Radio.Button>
                                                <Radio.Button value="MRS">{t('mrs')}</Radio.Button>
                                                <Radio.Button value="DR">{t('dr')}</Radio.Button>
                                            </Radio.Group>
                                        </Form.Item>
                                    </Col>
                                    <Col md={{ span: 24 }} xs={{ span: 24 }}>
                                        <Form.Item name="guestName" rules={rules.guestName} label={t('guest_name')}>
                                            <Input />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row gutter={[5, 5]}>
                                    <Col md={{ span: 12 }} xs={{ span: 24 }}>
                                        <Form.Item
                                            name="guestZipCode"
                                            label={t('guest_zipcode')}
                                        >
                                            <Input />
                                        </Form.Item>
                                    </Col>
                                    <Col md={{ span: 12 }} xs={{ span: 24 }}>
                                        <Form.Item
                                            name="guestCountry"
                                            label={t('guest_country')}
                                        >
                                            <ReactFlagsSelect searchable={true} selected={countryCode} onSelect={(code) => setCountryCode(code)} placeholder={t('guest_country')} className="country-select" />
                                        </Form.Item>
                                    </Col>

                                    <Col md={{ span: 12 }} xs={{ span: 24 }}>
                                        <Form.Item
                                            name="guestCity"
                                            label={t('guest_city')}
                                        >
                                            <Input />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                            <Col md={{ span: 12 }} xs={{ span: 24 }}>
                                <Title level={3}>{t('booking.payment.title')}</Title>
                                <span style={{ display: 'block', marginBottom: 6 }}>{t('booking.payment.disabled')}</span>
                                <Row gutter={[5, 5]} style={{ paddingBottom: 24 }}>
                                    <Col md={{ span: 24 }} xs={{ span: 24 }}>
                                        <img className="payment-card" src={require('./../../assets/img/payment/visa.jpg')} alt="Visa" />
                                        <img className="payment-card" src={require('./../../assets/img/payment/mastercard.jpg')} alt="Visa" />
                                        <img className="payment-card" src={require('./../../assets/img/payment/amex.jpg')} alt="Visa" />
                                        <img className="payment-card" src={require('./../../assets/img/payment/visaelectron.jpg')} alt="Visa" />
                                        <img className="payment-card" src={require('./../../assets/img/payment/unionpay.jpg')} alt="Visa" />
                                        <img className="payment-card" src={require('./../../assets/img/payment/jcb.jpg')} alt="Visa" />
                                    </Col>
                                </Row>
                                <Row gutter={[5, 5]}>
                                    <Col md={{ span: 24 }} xs={{ span: 24 }}>
                                        <Form.Item
                                            name="paymentNumber"
                                            label={t('booking.payment.number')}
                                        >
                                            <Input disabled />
                                        </Form.Item>
                                    </Col>
                                </Row>
                                <Row gutter={[5, 5]}>
                                    <Col md={{ span: 24 }} xs={{ span: 24 }}>
                                        <Form.Item
                                            label={t('booking.payment.name')}
                                        >
                                            <Input disabled />
                                        </Form.Item>
                                    </Col>

                                    <Col md={{ span: 12 }} xs={{ span: 24 }}>
                                        <Form.Item
                                            label={t('booking.payment.expiredMonth')}
                                        >
                                            <Input disabled />
                                        </Form.Item>
                                    </Col>
                                    <Col md={{ span: 12 }} xs={{ span: 24 }}>
                                        <Form.Item
                                            label={t('booking.payment.expiredYear')}
                                        >
                                            <Input disabled />
                                        </Form.Item>
                                    </Col>
                                </Row>
                            </Col>
                        </Row>
                    </Card>}
                    {currentStep === 2 && <Card title={t('booking.step_4')} style={{ width: '100%' }} type="inner">
                        <Descriptions>
                            <Descriptions.Item label={t('guest_email')}>{bookingData.guestEmail}</Descriptions.Item>
                            <Descriptions.Item label={t('guest_name')}>{bookingData.guestTitle} {bookingData.guestName}</Descriptions.Item>
                            <Descriptions.Item label={t('guest_phone')}>{bookingData.guestPhone}</Descriptions.Item>
                            <Descriptions.Item label={t('guest_zipcode')}>{bookingData.guestZipCode}</Descriptions.Item>
                            <Descriptions.Item label={t('guest_city')}>{bookingData.guestCity}</Descriptions.Item>
                            <Descriptions.Item label={t('guest_country')}>{bookingData.guestCountry}</Descriptions.Item>
                        </Descriptions>
                    </Card>}
                </Space>

            </Form>
        </Modal>
    );
};

export default GuestInfoModal;