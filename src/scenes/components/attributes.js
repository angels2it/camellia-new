import React, { useState } from 'react';
import _ from 'lodash';
import { Row, Col, Tooltip, Card } from 'antd';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { formatRoomAttribute } from '../../helpers/utils';

const Attributes = ({ items: attributes, onAttributeClick }) => {
    if (_.isEmpty(attributes))
        return null;
    return (

        <Row gutter={[10, 10]} style={{ marginTop: 10 }} justify="space-between" align="stretch">
            {attributes.map((a, index) =>
                <Col key={index} span={8} onClick={() => onAttributeClick(a)} style={{ cursor: 'pointer' }}>
                    <Tooltip title={formatRoomAttribute(a)}>
                        <FontAwesomeIcon style={{ fontSize: 20, verticalAlign: 'middle' }} icon={a.attribute.icon} />
                    </Tooltip>
                    <span style={{ verticalAlign: "middle", marginLeft: 10 }}>{formatRoomAttribute(a)}</span>
                </Col>
            )}
        </Row>
    )
}
export default Attributes;