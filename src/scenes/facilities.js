import React, { useContext, useEffect, useState } from "react";
import _ from "lodash";
import resortService from "./../services/resort";

import Header from "./../components/header";
import Banner from "./../components/banner";
import Footer from "./../components/footer";
import FacilityItem from "./../components/facility-item";
import Helmet from "react-helmet";
import { useTranslation } from "react-i18next";
import SettingContext from "../contexts/setting";

function Facilities({ changeLanguage }) {
  const [items, setItems] = useState([]);
  const setting = useContext(SettingContext);
  const { t } = useTranslation();
  useEffect(() => {
    resortService.getFacilities().then((result) => {
      setItems(_.get(result, "items", []));
    });
  }, []);
  return (
    <>
      <Header changeLanguage={changeLanguage} />
      <Helmet>
        <title>{t('facilities')}</title>
      </Helmet>
      <main>
        <Banner />
        <section className="cards">
          <div className="container">
            <div className="row">
              {_.map(items, (item, index) => (
                <div
                  key={index}
                  className={
                    index === 0 ? "col-xs-12 col-md-8" : "col-xs-6 col-md-4"
                  }
                >
                  <FacilityItem item={item} locale={setting.locale}></FacilityItem>
                </div>
              ))}
            </div>
          </div>
        </section>
      </main>
      <Footer />
    </>
  );
}

export default Facilities;
