class Config {
    
    setLocale(newLocale) {
        this.locale = newLocale;
    }
}

export default new Config();