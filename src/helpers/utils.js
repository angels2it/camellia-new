import _ from 'lodash';

export const formatCurrency = (value, currency = 1) =>
  Intl.NumberFormat("vi-VN", {
    style: "currency",
    currency: currency,
  }).format(value);

export const countDiscount = (price, discount, type) => {
  const discountValue = type === 'PERCENT' ? (discount * price) / 100 : discount;
  return price - discountValue;
};

export const formatRoomAttribute = (roomAttribute) => {
  if (_.isEmpty(roomAttribute.value)) {
    return roomAttribute.attribute.name
  }
  return `${roomAttribute.attribute.name} - ${roomAttribute.value}`
}