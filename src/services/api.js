import axios from "axios";
import config from "./../helpers/config";

const api = axios.create({
  baseURL: process.env.API_URL,
});

api.interceptors.request.use(function (request) {
  request.headers['.AspNetCore.Culture'] = config.locale;
  return request;
})

api.interceptors.response.use(function (response) {
  if (response.data && response.data.success) {
    return response.data.result;
  }
  return null;
});

export default api;
