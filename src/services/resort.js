import _ from 'lodash';
import api from "./api";

class Room {
  getRooms(input) {
    return api.post("api/services/app/Rooms/search", input);
  }

  getAllRooms(input) {
    return api.get("api/services/app/Rooms", { params: input });
  }

  booking(input) {
    return api.post(`api/services/app/Rooms/booking`, input);
  }

  getFacilities(type, take, exceptId) {
    return api.get("api/services/app/Facilities", {
      params: _.omitBy({ type: type, maxResultCount: take, exceptId }, _.isNil),
    });
  }

  getServices(take) {
    return api.get("api/services/app/Services", {
      params: { maxResultCount: take },
    });
  }

  getNews(take, exceptId = 0) {
    return api.get("api/services/app/News", {
      params: { maxResultCount: take, exceptId },
    });
  }

  getBySlug(slug) {
    return api.get(`api/services/app/Pages/slug/${slug}`);
  }
  getFacility(id) {
    return api.get(`api/services/app/Facilities/${id}`);
  }
  getNewsItem(id) {
    return api.get(`api/services/app/News/${id}`);
  }
  getRoomById(id) {
    return api.get(`api/services/app/Rooms/${id}`);
  }

  getRoomWithCurrencyById(id, currency) {
    return api.get(`api/services/app/Rooms/${id}/${currency}`);
  }

  getSettings() {
    return api.get(`api/services/app/Configuration/ui-settings`);
  }

  getAlbums(type = 'SLIDE', refId, refType) {
    return api.get(`api/services/app/Albums/${type}`, {
      params: _.omitBy({ refId, refType }, _.isNil)
    });
  }

  getAlbumsTypes() {
    return api.get(`api/services/app/Albums/types/all`);
  }

  sendFeedback(data) {
    return api.post(`api/services/app/Feedbacks`, data);
  }
}

export default new Room();
