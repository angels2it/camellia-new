import React, { useContext } from "react";
import OwlCarousel from "react-owl-carousel";
import _ from "lodash";
import { useTranslation } from "react-i18next";
import SettingContext from "../contexts/setting";
import RoomItem from './room-item';

function Rooms({ items = [] }) {
  const { t } = useTranslation();
  const setting = useContext(SettingContext);
  return (
    <section className="rooms rooms-widget">
      <div className="section-header">
        <div className="container">
          <h2 className="title">
            {setting.locale === 'en' && (<><span>Rooms</span> ACCOMMODATION</>)}
            {setting.locale === 'vi' && (<><span>PHÒNG</span> KHÁCH SẠN</>)}

            <a href={`/${setting.locale}/rooms`} className="btn btn-sm btn-clean-dark">
              {t('more')}
            </a>
          </h2>
          <p>
            {t('rooms_detail')}
          </p>
        </div>
      </div>
      <div className="container">
        {!_.isEmpty(items) && (
          <OwlCarousel
            className="owl-room owl-theme"
            loop
            nav
            lazyLoad={true}
            lazyContent={true}
            autoplay
            autoplayTimeout={4000}
            autoplayHoverPause
            dots
            margin={10}
            responsive={{
              0: {
                items: 1,
              },
              600: {
                items: 1,
              },
              1000: {
                items: 3,
              },
            }}
          >
            {_.map(
              items,
              (item) => (
                <div className="item" key={item.id.toString()}>
                  <RoomItem item={item} locale={setting.locale} />
                </div>
              )
            )}
          </OwlCarousel>
        )}
      </div>
    </section>
  );
}

export default Rooms;
