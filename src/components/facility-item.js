import React from 'react';
import slugify from 'slugify';

const FacilityItem = ({ item, locale }) => {
  if (!item || !item.title) {
    return null;
  }
  return (
    <figure>
      <figcaption
        style={{
          backgroundImage: `url(${item.thumbnail})`,
        }}
        className="growimg"
      >
        <img src={item.thumbnail} alt={item.title} />
      </figcaption>
      <a href={`/${locale}/facility/${item.id}/${slugify(item.title, { lower: true })}`} className="btn btn-clean">
        {item.title}
      </a>
    </figure>
  );
};

export default FacilityItem;