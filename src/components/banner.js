import React, { useContext, useEffect, useMemo, useState } from "react";
import OwlCarousel from "react-owl-carousel";

import QuickBooking from "./quick-booking";
import SettingContext from './../contexts/setting';

const Banner = () => {
  const setting = useContext(SettingContext);
  var slides = useMemo(() => {
    return _.filter(_.get(setting, 'documents', []), d => d.type === 'SLIDE');
  }, [setting])
  return (
    <div className="banner">
      {_.size(slides) > 0 && <OwlCarousel
        className="owl-theme"
        loop
        nav
        dots
        responsive={{
          0: {
            items: 1,
          },
          600: {
            items: 1,
          },
          1000: {
            items: 1,
          },
        }}
        id="sliderBanner"
      >
        {slides.map((s, index) => <div key={index} className="item">
          <div className="banner__item">
            <div className="banner__img">
              <img src={s.url} alt="" />
            </div>
            <div className="banner__content">
              <div className="container">
                <h1 className="title">
                  Camellia Tam Đảo
                <br />
                </h1>
              </div>
            </div>
          </div>
        </div>
        )}
      </OwlCarousel>}
      <QuickBooking />
    </div>
  )
};

export default Banner;
