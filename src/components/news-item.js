import React from "react";
import moment from 'moment';
import slugify from 'slugify';

const NewsItems = ({ item, locale }) => {
  if (item == null || item.title === null) {
    return null;
  }
  const date = moment.utc(item.creationTime).local()
  return (
    <a href={`/${locale}/news/${item.id}/${slugify(item.title, { lower: true })}`}>
      <article>
        <div className="image grow">
          <img src={item.thumbnail} alt={item.title} />
        </div>
        <div className="text">
          <div className="info">
            {/* {item.creatorUser && <span>{item.creatorUser.name}</span>}{' • '} */}
            <span>{date.format('hh:mm DD/MM/YYYY')}</span>
          </div>
          <div className="time">
            <span>{date.format('DD')}</span>
            <span>{date.format('MM')}</span>
            <span>{date.format('YYYY')}</span>
          </div>
          <h4>{item.title}</h4>
          <div className="description">
            <p className="news-description">{item.description}</p>
          </div>
        </div>
      </article>
    </a>
  );
};
export default NewsItems;
