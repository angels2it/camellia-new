import DatePicker from "react-datepicker";
import moment from "moment";
import { Row, Col, Popover, Button, List } from "antd";
import React, { forwardRef, useContext, useEffect, useState } from "react";
import { useHistory } from "react-router-dom";
import qs from "query-string";
import { useTranslation } from 'react-i18next';
import "./quick-booking.scss";
import "react-datepicker/dist/react-datepicker.css";
import SettingContext from './../contexts/setting';

const DateInput = forwardRef(({ value, onClick }, ref) => {
  const date = moment(value, "MM/DD/YYYY");
  return (
    <>
      <div className="date-value" onClick={onClick}>
        <span className="day">{date.format("DD")}</span>
        <span className="month">{date.format("MMM")}</span>
        <span className="year">{date.format("YYYY")}</span>
      </div>
    </>
  );
});

const GuestPopup = ({ onChange }) => {
  const {t} = useTranslation();
  const [adults, setAdults] = useState(1);
  const [childrens, setChildrens] = useState(1);

  useEffect(() => {
    onChange && onChange({ adults, childrens });
  }, [adults, childrens]);
  return (
    <List className="guest-popup">
      <List.Item
        actions={[
          <Button key="subtract" onClick={() => setAdults(adults - 1)}>
            -
          </Button>,
          <Button key="plus" onClick={() => setAdults(adults + 1)}>
            +
          </Button>,
        ]}
      >
        <List.Item.Meta
          avatar={<span className="people-number">{adults}</span>}
          title={t('adults')}
          description={t('adults_description')}
        />
      </List.Item>
      <List.Item
        actions={[
          <Button key="subtract" onClick={() => setChildrens(childrens - 1)}>
            -
          </Button>,
          <Button key="plus" onClick={() => setChildrens(childrens + 1)}>
            +
          </Button>,
        ]}
      >
        <List.Item.Meta
          avatar={<span className="people-number">{childrens}</span>}
          title={t('childrens')}
          description={t('childrens_description')}
        />
      </List.Item>
    </List>
  );
};

function QuickBooking() {
  const {t} = useTranslation();
  const setting = useContext(SettingContext);
  const [checkInDate, setCheckInDate] = useState(
    moment().startOf("d").toDate()
  );
  const [checkOutDate, setCheckOutDate] = useState(
    moment().add(1, "d").endOf("d").toDate()
  );
  const [isGuestVisible, setIsGuestVisible] = useState(false);
  const [people, setPeople] = useState({ numOfAdults: 1, numOfChildrens: 1 });
  const history = useHistory();
  const guestChangeHandler = (value) => {
    setPeople({ numOfAdults: value.adults, numOfChildrens: value.childrens });
  };

  const booknowHandler = () => {
    const query = qs.stringify({
      checkInDate: moment(checkInDate).utc().format(),
      checkOutDate: moment(checkOutDate).utc().format(),
      ...people,
    });
    history.push(`/${setting.locale}/booking?${query}`);
  };
  return (
    <div className="banner__booking">
      <div className="container">
        <div className="row align-items-center">
          <div className="col-md-4 col-4">
            <div className="book-date">
              <label>{t('arrival')} <i className="fas fa-long-arrow-alt-right" aria-hidden="true" /></label>
              <DatePicker
                selected={checkInDate}
                minDate={moment().toDate()}
                onChange={(date) => setCheckInDate(date)}
                customInput={<DateInput />}
              />
            </div>
          </div>
          <div className="col-md-4 col-4">
            <div className="book-date">
              <label>{t('departure')} <i className="fas fa-long-arrow-alt-right" aria-hidden="true" /></label>
              <input type="text" id="to-date" />
              <DatePicker
                selected={checkOutDate}
                minDate={checkInDate}
                onChange={(date) => setCheckOutDate(date)}
                customInput={<DateInput />}
              />
            </div>
          </div>
          <div className="col-md-2 col-4">
            <div className="book-date">
              <label className="guest">
                {t('guests')}
              </label>
              <Popover
                content={<GuestPopup onChange={guestChangeHandler} />}
                title={t('guests_title')}
                trigger="click"
                visible={isGuestVisible}
                onVisibleChange={() => setIsGuestVisible(!isGuestVisible)}
              >
                <div className="date-value">
                  <span className="day">
                    {people.numOfAdults + people.numOfChildrens}
                  </span>
                </div>
              </Popover>
            </div>
          </div>
          <div className="col-md-2 col-12">
            <div className="book-now">
              <a
                onClick={booknowHandler}
                className="mt-4 button btn--big"
                id="bookNow"
              >
                <span>
                  {t('book_now')}
                  <small>{t('book_now_description')}</small>
                </span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default QuickBooking;
