import React, { useContext, useEffect, useState } from "react";
import _ from "lodash";
import { useTranslation } from "react-i18next";
import resort from "../services/resort";
import { useHistory } from "react-router-dom";
import SettingContext from "../contexts/setting";
import slugify from "slugify";

const StretcherWrapper = () => {
  const history = useHistory();
  const { t } = useTranslation();
  const [items, setItems] = useState([]);
  const setting = useContext(SettingContext);
  useEffect(() => {
    resort.getFacilities("RESORT", 4).then((data) => {
      setItems(_.get(data, "items", []));
    });
  }, []);
  const mouseEnterHandler = (e) => {
    e.target.parentNode.classList.add("active");
    e.target.parentNode.nextElementSibling &&
      e.target.parentNode.nextElementSibling.classList.add("inactive");
    e.target.parentNode.previousElementSibling &&
      e.target.parentNode.previousElementSibling.classList.add("inactive");
    //   document.querySelector(".stretcher-item.more").removeClass("inactive");
  };

  const mouseLeaveHandler = (e) => {
    e.target.parentNode.classList.remove("active");
    e.target.parentNode.nextElementSibling &&
      e.target.parentNode.nextElementSibling.classList.remove("inactive");
    e.target.parentNode.previousElementSibling &&
      e.target.parentNode.previousElementSibling.classList.remove("inactive");
  };

  const itemClickHandler = (item) => {
    if (item && item.title) {
      history.push(`/${setting.locale}/facility/${item.id}/${slugify(item.title)}`);
    }
  };

  return (
    <section className="stretcher-wrapper">
      <div className="section-header">
        <div className="container">
          <h2 className="title">
            {setting.locale === 'en' && <><span>RESORT</span> facilities</>}
            {setting.locale === 'vi' && <><span>TIỆN ÍCH</span> KHÁCH SẠN</>}
            <a href={`/${setting.locale}/facilities`} className="btn btn-sm btn-clean-dark">
              {t("more")}
            </a>
          </h2>
          <p>{t("facilities_description")}</p>
        </div>
      </div>
      <ul className="stretcher">
        {_.map(items, (item) => (
          <li
            onClick={() => itemClickHandler(item)}
            key={item.id}
            className="stretcher-item"
            onMouseEnter={mouseEnterHandler}
            onMouseLeave={mouseLeaveHandler}
            style={{
              backgroundImage: `url(${item.thumbnail})`,
            }}
          >
            <div className="stretcher-logo">
              <div className="text">
                <span className="text-intro h4">{item.title}</span>
              </div>
            </div>
            {/*main text*/}
            <figure>
              <h4>{item.title}</h4>
              <figcaption>{item.description}</figcaption>
            </figure>
            {/*anchor*/}
            <a href="/facility">Anchor link</a>
          </li>
        ))}
        {/* === stretcher item more === */}
        <li className="stretcher-item more">
          <div className="more-icon">
            <span data-title-show="Show more" data-title-hide="+">
              +
            </span>
            <a href={`${setting.locale}/facilities`}>{t("more")}</a>
          </div>
        </li>
      </ul>
    </section>
  );
};

export default StretcherWrapper;
