import React, { useEffect, useState, useContext } from "react";
import _ from "lodash";
import resort from "../services/resort";
import FacilityItem from "./facility-item";
import { useTranslation } from "react-i18next";
import SettingContext from "../contexts/setting";

const FacilitiesMobile = () => {
  const { t } = useTranslation();
  const [items, setItems] = useState([]);
  const setting = useContext(SettingContext);
  useEffect(() => {
    resort.getFacilities("RESORT", 4).then((data) => {
      setItems(_.get(data, "items", []));
    });
  }, []);
  return (
    <section className="cards">
      <div className="section-header">
        <div className="container">
          <h2 className="title">
            {setting.locale === 'en' && <><span>RESORT</span> facilities</>}
            {setting.locale === 'vi' && <><span>TIỆN ÍCH</span> KHÁCH SẠN</>}
            <a href={`/facilities`} className="btn btn-sm btn-clean-dark">
              {t('more')}
            </a>
          </h2>
          <p>
            {t('facilities_description')}
          </p>
        </div>
      </div>
      <div className="container">
        <div className="row">
          {_.map(items, (item, index) => (
            <div
              key={index}
              className={
                index === 0 ? "col-xs-12 col-md-8" : "col-xs-6 col-md-4"
              }
            >
              <FacilityItem item={item} locale={setting.locale}></FacilityItem>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default FacilitiesMobile;
