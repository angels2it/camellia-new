import React, { useContext, useEffect, useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { Drawer } from 'antd';
import _ from "lodash";
import i18next from "i18next";
import moment from 'moment';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useRouteMatch,
  useLocation,
} from "react-router-dom";
import SettingContext from "./../contexts/setting";

function Header({ changeLanguage, defaultSticky = false }) {
  const [isSticky, setSticky] = useState(defaultSticky);
  const { t } = useTranslation();
  const setting = useContext(SettingContext);
  const [menuVisible, setMenuVisible] = useState(false);
  const match = useRouteMatch('/:locale');
  const location = useLocation();

  const handleScroll = () => {
    if (defaultSticky) {
      return;
    }
    const offsetScroll = window.scrollY;
    if (offsetScroll > 50) setSticky(true);
    else setSticky(false);
  };

  useEffect(() => {
    window.addEventListener("scroll", handleScroll);
    return () => window.removeEventListener("scroll", () => handleScroll);
  }, [isSticky]);

  const onChangeLanguage = (e, locale) => {
    e.preventDefault();
    i18next.changeLanguage(locale);
    localStorage.setItem('locale', locale);
    moment.locale(locale);
    changeLanguage(locale, _.replace(location.pathname, `\/${setting.locale}`, ''));
  };

  const toogleMenu = () => {
    setMenuVisible(!menuVisible);
  }

  const leftMenu = useMemo(() => {
    return _(setting.menus).filter(m => m.type === 'HEADER_LEFT').sortBy('index').value()
  }, [setting]);
  const rightMenu = useMemo(() => {
    return _(setting.menus).filter(m => m.type === 'HEADER_RIGHT').sortBy('index').value()
  }, [setting]);

  return (
    <header>
      <div
        className={`header ${isSticky ? "active" : "not-active"}`}
        id="header"
      >
        <div className="container">
          <div className="nav_header">
            <div className="nav-top">
              <div className="nav-top__left">
                {!_.isEmpty(setting["App.UI.Social.Facebook.Url"]) && (
                  <a href={setting["App.UI.Social.Facebook.Url"]} target="_bank">
                    <i className="fab fa-facebook-f" />
                  </a>
                )}
                {!_.isEmpty(setting["App.UI.Social.Twitter.Url"]) && (
                  <a href={setting["App.UI.Social.Twitter.Url"]} target="_bank">
                    <i className="fab fa-twitter" />
                  </a>
                )}
                {!_.isEmpty(setting["App.UI.Social.Youtube.Url"]) && (
                  <a href={setting["App.UI.Social.Youtube.Url"]} target="_bank">
                    <i className="fab fa-youtube" />
                  </a>
                )}
              </div>
              <div className="nav-top__right">
                {/* <a href="#">
                  <i className="fas fa-star" /> Special offers
                </a> */}
                {/* <a href="#">
                  <i className="fas fa-tags" /> Reservations
                </a> */}
                {setting["App.UI.Phone"] && (
                  <a href={`tel: ${setting["App.UI.Phone"]}`}>
                    <i className="fas fa-phone" /> {setting["App.UI.Phone"]}
                  </a>
                )}
              </div>
            </div>
          </div>
          <div className="nav_menu">
            <div className="menu_left">
              <ul className="listmenu">
                {leftMenu.map(m => (
                  <li key={m.code}>
                    <a href={`/${setting.locale}${m.url}`}>{m.name}</a>
                  </li>
                ))}
              </ul>
            </div>
            <div className="logo_header">
              <a href="/home">
                <img src={require("./../assets/img/logo/logo.png")} alt="" />
              </a>
            </div>
            <div className="menu_right">
              <ul className="listmenu">
                {rightMenu.map(m => (
                  <li key={m.code}>
                    <a href={`/${setting.locale}${m.url}`}>{m.name}</a>
                  </li>
                ))}
                <li>
                  <a href="#">
                    <img src={require('./../assets/img/logo/vietnam.png')} onClick={(e) => onChangeLanguage(e, "vi")} aria-hidden="true" />
                  </a>
                  <a href="#">
                    <img src={require('./../assets/img/logo/united-kingdom.png')} onClick={(e) => onChangeLanguage(e, "en")} aria-hidden="true" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div className="menumobile">
            <div className="navbar-toogle" onClick={toogleMenu}>
              <i className="fas fa-bars" />
            </div>
            <Drawer
              placement="left"
              closable={true}
              onClose={toogleMenu}
              className="menumobilecontent"
              bodyStyle={{ backgroundColor: '#343a40' }}
              visible={menuVisible}
            >
              <ul className="list_menu_mobile">
                <li>
                  <a href="/">{t("home")}</a>
                </li>
                <li>
                  <a href="/pages/about">{t("about")}</a>
                </li>
                <li>
                  <a href="/rooms">{t('menu_rooms')}</a>
                  {/* <ul className="listdrop_mobile">
                    {_.map(roomTypes, (r, index) => (<li key={index}>
                      <a href={`/rooms?type=${r.id}`}>
                        {r.name}
                      </a>
                    </li>))}
                  </ul> */}
                </li>

                <li>
                  <a href="/facilities">{t("facilities")}</a>
                </li>
                <li>
                  <a href="/news">{t("news")}</a>
                </li>
                <li>
                  <a href="/albums">{t("albums")}</a>
                </li>
                <li>
                  <a href="/pages/contact">{t("contact")}</a>
                </li>
              </ul>
            </Drawer>
            <div className="menumobileright">
              <ul className="listmenu">
                <li>
                  <a href="#">
                    <img src={require('./../assets/img/logo/vietnam.png')} onClick={(e) => onChangeLanguage(e, "vi")} aria-hidden="true" />
                  </a>
                  <a href="#">
                    <img src={require('./../assets/img/logo/united-kingdom.png')} onClick={(e) => onChangeLanguage(e, "en")} aria-hidden="true" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </header>
  );
}

export default Header;
