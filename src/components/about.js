import React from 'react';
import { Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';

const ImageBlock = () => {
    const { t } = useTranslation();
    return (
        <div className="image-blocks image-blocks-header">
            <div
                className="section-header"
                style={{
                    backgroundImage: `url(${require("./../assets/img/detail/header-1.jpg")})`,
                }}
            >
                <div className="container">
                    <h2 className="title">
                        {t('about_header')}
                    </h2>
                    <p>{t('about_subheader')}</p>
                </div>
            </div>
            <div className="container">
                <div className="blocks_content">
                    <div className="blocks">
                        <Row>
                            <Col span={{ md: 12, sm: 24 }}>
                                <div className="blocks_img">
                                    <img
                                        src={require("./../assets/img/detail/apartment-1.jpg")}
                                        alt=""
                                    />
                                </div>
                            </Col>
                            <Col span={{ md: 12, sm: 24 }}>
                                <div className="blocks_text">
                                    <div className="text">
                                        {/* === room info === */}
                                        <h2 className="title">
                                            {t('about_title')}
                                        </h2>
                                        <p>
                                            {t('about_description')}
                                        </p>
                                    </div>
                                </div>
                            </Col>
                        </Row>

                    </div>
                </div>
            </div>
        </div>
    )
};

export default ImageBlock;