import React, { useState, useEffect, useContext } from "react";
import _ from "lodash";
import { useTranslation } from "react-i18next";
import resort from "../services/resort";
import NewsItem from "./news-item";
import SettingContext from "../contexts/setting";

const News = () => {
  const [items, setItems] = useState([]);
  const { t } = useTranslation();
  const setting = useContext(SettingContext);
  useEffect(() => {
    resort.getNews(3).then((data) => {
      setItems(_.get(data, "items", []));
    });
  }, []);
  return (
    <section className="new">
      <div className="section-header">
        <div className="container">
          <h2 className="title">
            {setting.locale === 'en' && <><span>LATEST</span> NEWS</>}
            {setting.locale === 'vi' && <><span>TIN TỨC</span> MỚI NHẤT</>}
            <a href={`/${setting.locale}/news`} className="btn btn-sm btn-clean-dark">
              {t("more")}
            </a>
          </h2>
          <p>{t('news_detail')}</p>
        </div>
      </div>
      <div className="container">
        <div className="row">
          {_.map(items, (item) => (
            <div className="col-md-4" key={item.id}>
              <NewsItem item={item} />
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default News;
