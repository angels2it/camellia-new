import React, { useEffect, useState, useContext } from "react";
import _ from "lodash";
import resort from "../services/resort";
import FacilityItem from "./facility-item";
import { useTranslation } from "react-i18next";
import SettingContext from './../contexts/setting'

const NearBy = () => {
  const { t } = useTranslation();
  const setting = useContext(SettingContext)
  const [items, setItems] = useState([]);
  useEffect(() => {
    resort.getFacilities("NEARBY", 5).then((data) => {
      setItems(_.get(data, "items", []));
    });
  }, []);
  return (
    <section className="cards">
      <div className="section-header">
        <div className="container">
          <h2 className="title">
            <span>{t('nearby')}</span>
            <a href={`${setting.locale}/facilities`} className="btn btn-sm btn-clean-dark">
              {t('more')}
            </a>
          </h2>
          <p>
            {t('nearby_detail')}
          </p>
        </div>
      </div>
      <div className="container">
        <div className="row">
          {_.map(items, (item, index) => (
            <div
              key={index}
              className={
                index === 0 ? "col-xs-12 col-md-8" : "col-xs-6 col-md-4"
              }
            >
              <FacilityItem item={item} locale={setting.locale}></FacilityItem>
            </div>
          ))}
        </div>
      </div>
    </section>
  );
};

export default NearBy;
