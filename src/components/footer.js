import React, { useContext, useMemo } from "react";
import { useTranslation } from "react-i18next";
import _ from "lodash";
import {
  EmailIcon,
  FacebookIcon,
  FacebookMessengerIcon,
  HatenaIcon,
  InstapaperIcon,
  LineIcon,
  LinkedinIcon,
  LivejournalIcon,
  MailruIcon,
  OKIcon,
  PinterestIcon,
  PocketIcon,
  RedditIcon,
  TelegramIcon,
  TumblrIcon,
  TwitterIcon,
  ViberIcon,
  VKIcon,
  WeiboIcon,
  WhatsappIcon,
  WorkplaceIcon
} from "react-share";
import SettingContext from "./../contexts/setting";
import { Row, Col } from "antd";

const Footer = () => {
  const { t } = useTranslation();
  const setting = useContext(SettingContext);
  const leftMenu = useMemo(() => {
    return _(setting.menus).filter(m => m.type === 'FOOTER_LEFT').sortBy('index').value()
  }, [setting]);
  return (
    <footer>
      <div className="container">
        <div className="footer-info">
          <Row align="bottom">
            <Col md={10} xs={24} className="footer-left">
              <img className="footer-logo" alt="logo" src="/images/logo.15a99db3246d149e7313ca5ca30b87df.png" />
              <div style={{ marginLeft: 10, marginTop: 10 }}>
                {setting["App.UI.Name"] && (
                  <p>
                    {setting["App.UI.Name"]}
                  </p>
                )}
                {setting["App.UI.Phone"] && (
                  <p>
                    <a href={`tel: ${setting["App.UI.Phone"]}`}>
                      {t('resort.phone')}: {setting["App.UI.Phone"]}
                    </a>
                  </p>
                )}
                {setting["App.UI.Email"] && (
                  <p>
                    <a href={`mailto: ${setting["App.UI.Email"]}`}>
                      {t('resort.email')}: {setting["App.UI.Email"]}
                    </a>
                  </p>
                )}
                {setting["App.UI.Address"] && (
                  <p>
                    {t('resort.address')}: {setting["App.UI.Address"]}
                  </p>
                )}
                {setting["App.UI.TaxNumber"] && (
                  <p>
                    {t('resort.taxNumber')}: {setting["App.UI.TaxNumber"]}
                  </p>
                )}
              </div>
            </Col>
            <Col md={8} xs={24} className="footer-link">
              <ul>
                {leftMenu.map(m => (
                  <li key={m.code}>
                    <a href={`/${setting.locale}${m.url}`}>{m.name}</a>
                  </li>
                ))}
              </ul>
            </Col>
            <Col md={6} xs={24} className="footer-right icon">
              <span style={{ marginBottom: 20, display: 'block' }}>{t('social_icon')}</span>
              <ul>
                <li>
                  {!_.isEmpty(setting["App.UI.Social.Facebook.Url"]) && (
                    <a href={setting["App.UI.Social.Facebook.Url"]} target="_blank">
                      <FacebookIcon />
                    </a>
                  )}
                </li>
                <li>
                  {!_.isEmpty(setting["App.UI.Social.Twitter.Url"]) && (
                    <a href={setting["App.UI.Social.Twitter.Url"]} target="_blank">
                      <TwitterIcon />
                    </a>
                  )}
                </li>

                <li>
                  {!_.isEmpty(setting["App.UI.Social.Youtube.Url"]) && (
                    <a href={setting["App.UI.Social.Youtube.Url"]} target="_blank">
                      <img src={require('./../assets/img/logo/youtube.png')} />
                    </a>
                  )}
                </li>
                <li>
                  {!_.isEmpty(setting["App.UI.Social.Instagram.Url"]) && (
                    <a href={setting["App.UI.Social.Instagram.Url"]} target="_blank">
                      <img src={require('./../assets/img/logo/instagram.png')} />
                    </a>
                  )}
                </li>
              </ul>
              <div className="col-sm-12 copyright">
                <small>Copyright © 2020</small>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    </footer>
  );
};

export default Footer;
