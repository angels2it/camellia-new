import React from 'react';

const ServiceItem = ({ item, locale }) => {
  if (!item || !item.title) {
    return null;
  }
  return (
    <figure>
      <figcaption
        style={{
          backgroundImage: `url(${item.thumbnail})`,
        }}
      >
        <img src={item.thumbnail} alt="" />
      </figcaption>
      <a href={`/${locale}/service/${item.id}/${item.title}`} className="btn btn-clean">
        {item.title}
      </a>
    </figure>
  );
};

export default ServiceItem;