import React, { useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import _ from 'lodash';
import { countDiscount, formatCurrency, formatRoomAttribute } from "../helpers/utils";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Tooltip } from "antd";
import slugify from 'slugify';

const RoomItem = ({ item: {
    id,
    name,
    description,
    thumbnail,
    currency,
    attributes,
    pricingItems
}, locale } = {}) => {
    const { t } = useTranslation();
    const price = useMemo(() => {
        var minPriceItem = _.minBy(pricingItems, ({ price, discount, discountType, }) => {
            return countDiscount(price, discount, discountType);
        }) || {};
        return minPriceItem;
    }, [pricingItems]);
    return (
        <article>
            <div className="image grow">
                <img src={thumbnail} alt={name} />
            </div>
            <div className="details">
                <div className="text">
                    <h3 className="title">
                        <a href={`/${locale}/rooms/${id}/${name}`}>{name}</a>
                    </h3>
                    <div>
                        <span
                            className={
                                price.discount > 0 ? "price has-discount" : "price"
                            }
                        >
                            {formatCurrency(price.price, currency)}
                        </span>{" "}
                        {price.discount > 0 && (
                            <span className="price-discount">
                                {formatCurrency(
                                    countDiscount(price.price, price.discount, price.discountType),
                                    currency
                                )}
                            </span>
                        )}
                        <span className="text-small">{t('per_night')}</span>
                    </div>
                    {/* {bedType && <p>{bedType.name}</p>} */}
                    {/* {!_.isEmpty(attributes) &&
                        <p>{attributes.map((a, index) => <span key={index} className="room-attribute-icon"><Tooltip title={formatRoomAttribute(a)}><FontAwesomeIcon icon={a.attribute.icon} /></Tooltip></span>)}</p>
                    } */}
                    {description && <p className="room-description">{description}</p>}
                </div>
                <div className="book">
                    <div style={{ flex: 2, display: 'flex' }}>
                        <div>
                            <a href={`/${locale}/booking`} className="btn btn-main">
                                {t('book_now')}
                            </a>
                        </div>
                        <div>
                            <a href={`/${locale}/rooms/${id}/${slugify(name)}`} className="btn btn-sub">
                                {t('room.detail')}
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </article>
    )
}

export default RoomItem;