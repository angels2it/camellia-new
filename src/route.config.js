import Home from "./scenes/home";
import Booking from "./scenes/booking";
import Page from "./scenes/page";
import Albums from "./scenes/albums";
import NewsDetail from "./scenes/news-detail";
import Facilities from "./scenes/facilities";
import Services from "./scenes/services";
import News from "./scenes/news";
import Rooms from "./scenes/rooms";

const routes = [
  {
    path: "/:locale/home",
    component: Home,
  },
  {
    path: "/:locale/booking",
    component: Booking,
  },
  {
    path: "/:locale/facilities",
    component: Facilities,
  },
  {
    path: "/:locale/services",
    component: Services,
  },
  {
    path: "/:locale/:type/:id/",
    component: Page,
  },
  {
    path: "/:locale/albums",
    component: Albums,
  },
  {
    path: "/:locale/news",
    component: News,
  },
  {
    path: "/:locale/rooms",
    component: Rooms,
  },
  {
    path: "/:locale/news/:slug",
    component: NewsDetail,
  },
];
export default routes;
