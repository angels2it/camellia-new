require('dotenv').config()
const express = require('express');
var compression = require('compression');
const app = express();
const port = process.env.PORT || 5000;
const path = require('path');
const fs = require('fs')
const resortService = require('./services/resort').default;
const config = require('./helpers/config').default;

const url = process.env.API_URL;


const filePath = path.resolve(__dirname, '../build', 'index.html');
const indexData = fs.readFileSync(filePath, 'utf8');

app.use(compression());

const home = function (request, response) {
  console.log('Home page visited!');
  response.send(defaultHeader(indexData));
};

app.get('/', home);
app.get('/en', home);
app.get('/vi', home);
app.get('/:locale/home', home);

const defaultHeader = (data) => {
  data = data.replace(/\$OG_TITLE/g, 'Camellia');
  data = data.replace(/\$OG_DESCRIPTION/g, "Camellia Tam Đảo");
  return data.replace(/\$OG_IMAGE/g, url + '/img/index/slide-1.jpg');
}

const readFile = (callback, locale = 'vi') => {
  config.setLocale(locale);
  // api.defaults.headers.common['.AspNetCore.Culture'] = locale;
  callback(indexData);
}

const roomsController = function (request, response) {
  console.log('rooms page visited!', request.params);
  readFile(data => {
    data = data.replace(/\$OG_TITLE/g, 'Rooms');
    data = data.replace(/\$OG_DESCRIPTION/g, 'Rooms');
    var result = data.replace(/\$OG_IMAGE/g, url + '/img/index/slide-1.jpg');
    response.send(result);
  })
}

const newsController = function (request, response) {
  console.log('news page visited!', request.params);
  readFile(data => {
    data = data.replace(/\$OG_TITLE/g, 'News');
    data = data.replace(/\$OG_DESCRIPTION/g, 'News');
    var result = data.replace(/\$OG_IMAGE/g, url + '/img/index/slide-1.jpg');
    response.send(result);
  })
}

const facilitiesController = function (request, response) {
  console.log('facilities page visited!', request.params);
  readFile(data => {
    data = data.replace(/\$OG_TITLE/g, 'Facilities');
    data = data.replace(/\$OG_DESCRIPTION/g, 'Facilities');
    var result = data.replace(/\$OG_IMAGE/g, url + '/img/index/slide-1.jpg');
    response.send(result);
  })
}


const pagesController = function (request, response) {
  console.log('pages page visited!', request.params);
  readFile(data => {
    var id = request.params.id;
    resortService.getBySlug(id).then((pageData) => {
      if (pageData) {
        data = data.replace(/\$OG_TITLE/g, pageData.title);
        if (pageData.description) {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.description);
        } else {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.title);
        }
        var result = data.replace(/\$OG_IMAGE/g, pageData.thumbnail);
        response.send(result);

      } else {
        response.send(defaultHeader(data));
      }
    })
      .catch(err => {
        console.log(err);
        response.send(data);
      });
  })
};

app.get('/:locale/pages/:id', pagesController);
app.get('/:locale/news', newsController);
app.get('/:locale/rooms', roomsController);
app.get('/:locale/facilities', facilitiesController);

const roomItemController = function (request, response) {
  console.log('roomItem page visited!', request.params);
  readFile(data => {
    var id = request.params.id;
    resortService.getRoomById(id).then((pageData) => {
      if (pageData) {
        data = data.replace(/\$OG_TITLE/g, pageData.name);
        if (pageData.description) {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.description);
        } else {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.name);
        }
        var result = data.replace(/\$OG_IMAGE/g, pageData.thumbnail);
        response.send(result);

      } else {
        response.send(defaultHeader(data));
      }
    })
      .catch(err => {
        console.log(err)
        response.send(data);
      });
  }, request.params.locale)
};

const newsItemController = function (request, response) {
  console.log('newsItem page visited!', request.params);
  readFile(data => {
    var id = request.params.id;
    resortService.getNewsItem(id).then((pageData) => {
      if (pageData) {
        data = data.replace(/\$OG_TITLE/g, pageData.title);
        if (pageData.description) {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.description);
        } else {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.title);
        }
        var result = data.replace(/\$OG_IMAGE/g, pageData.thumbnail);
        response.send(result);

      } else {
        response.send(defaultHeader(data));
      }
    })
      .catch(err => {
        response.send(data);
      });
  }, request.params.locale)
};

const facilityItemController = function (request, response) {
  console.log('facilityItem page visited!', request.params);
  readFile(data => {
    var id = request.params.id;
    resortService.getFacility(id).then((pageData) => {
      if (pageData) {
        data = data.replace(/\$OG_TITLE/g, pageData.title);
        if (pageData.description) {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.description);
        } else {
          data = data.replace(/\$OG_DESCRIPTION/g, pageData.title);
        }
        var result = data.replace(/\$OG_IMAGE/g, pageData.thumbnail);
        response.send(result);

      } else {
        response.send(defaultHeader(data));
      }
    })
      .catch(err => {
        response.send(data);
      });
  }, request.params.locale)
};

app.get('/:locale/rooms/:id/:title', roomItemController);
app.get('/:locale/news/:id/:title', newsItemController);
app.get('/:locale/facility/:id/:title', facilityItemController);


app.get('/:locale/contact', function (request, response) {
  console.log('Contact page visited!');
  const filePath = path.resolve(__dirname, './build', 'index.html')
  fs.readFile(filePath, 'utf8', function (err, data) {
    if (err) {
      return console.log(err);
    }
    data = data.replace(/\$OG_TITLE/g, 'Contact Page');
    data = data.replace(/\$OG_DESCRIPTION/g, "Contact page description");
    var result = data.replace(/\$OG_IMAGE/g, 'https://i.imgur.com/V7irMl8.png');
    response.send(result);
  });
});

app.use(express.static(path.resolve(__dirname, '../build')));

app.get('robots.txt', function (request, response) {
  const filePath = path.resolve(__dirname, 'robots.txt');
  response.sendFile(filePath);
});

app.get('*', function (request, response) {
  const filePath = path.resolve(__dirname, '../build', 'index.html');
  response.sendFile(filePath);
});

app.listen(port, () => console.log(`Listening on port ${port}`));