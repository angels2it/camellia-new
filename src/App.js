import React, { useEffect, useState } from "react";
import routes from "./route.config";
import "antd/dist/antd.css";
import moment from 'moment';
import './i18n'
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
  useRouteMatch,
  useLocation,
} from "react-router-dom";

import { library } from '@fortawesome/fontawesome-svg-core'
import { fas } from '@fortawesome/free-solid-svg-icons'
import _ from 'lodash';

import SettingContext from "./contexts/setting";
import resort from "./services/resort";
import config from './helpers/config';

library.add(fas)

const locale = localStorage.getItem('locale') || 'en';
moment.locale(locale);

const App = () => {
  const [setting, setSetting] = useState({ locale, currency: locale === 'vi' ? "VND" : "USD" });

  useEffect(() => {
    config.setLocale(locale);
  }, []);

  const changeLanguage = (locale, url) => {
    localStorage.setItem('locale', locale);
    setSetting({ ...setting, locale, currency: locale === 'vi' ? "VND" : "USD" });
    window.location.href = `/${locale}${url}`;
  }

  useEffect(() => {
    resort.getSettings().then((data) => {
      setSetting(
        {
          ...setting,
          ..._.omit(data, ['ui']),
          ..._.defaults(setting.ui,
            _.mapValues(
              _.mapKeys(data.ui, (e) => e.name),
              (e) => e.value
            ))
        }
      );
    });
  }, [])
  return (

    <Router>
      <Switch>
        {routes.map((route, i) => (
          <Route
            key={i}
            path={route.path}
            render={(props) => (
              // pass the sub-routes down to keep nesting
              <SettingContext.Provider value={setting}>
                <route.component {...props} changeLanguage={changeLanguage} routes={route.routes} />
              </SettingContext.Provider>
            )}
          />
        ))}
        <Route path="/">
          <Redirect to={`/${locale}/home`} />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
